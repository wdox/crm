package com.wzz.crm.base;


import org.springframework.web.bind.annotation.ModelAttribute;

import javax.servlet.http.HttpServletRequest;

/**
 * 控制器基类
 *
 */
public class BaseController {


    //定义request域的变量
    @ModelAttribute
    public void preHandler(HttpServletRequest request){
        request.setAttribute("ctx", request.getContextPath());
    }


    //成功快捷返回
    public ResultInfo success(){
        return new ResultInfo();
    }

    //带消息的成功返回
    public ResultInfo success(String msg){
        ResultInfo resultInfo= new ResultInfo();
        resultInfo.setCode(200);
        resultInfo.setMsg(msg);
        return resultInfo;
    }

    //带消息的诗句域的返回
    public ResultInfo success(String msg,Object result){
        ResultInfo resultInfo= new ResultInfo();
        resultInfo.setMsg(msg);
        resultInfo.setCode(200);
        resultInfo.setResult(result);
        return resultInfo;
    }
}
