package com.wzz.crm.base;


/**
 * 查询的基类
 */
public class BaseQuery {
    private Integer page=1;     //当前页
    private Integer limit=10;   //每页记录数

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }
}
