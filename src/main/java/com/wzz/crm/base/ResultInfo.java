package com.wzz.crm.base;

/**
 * 返回结果模板
 * 前后端交互协议
 */
public class ResultInfo {
    //code码
    private Integer code=200;
    //返回消息
    private String msg="success";
    //返回的数据据data
    private Object result;

    public Object getResult() {
        return result;
    }

    public void setResult(Object result) {
        this.result = result;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
