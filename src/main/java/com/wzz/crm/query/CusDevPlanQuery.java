package com.wzz.crm.query;

import com.wzz.crm.base.BaseQuery;

/**
 * 客户开发计划的查询类
 */
public class CusDevPlanQuery extends BaseQuery {
    private Integer sid; //营销机会id

    public Integer getSid() {
        return sid;
    }

    public void setSid(Integer sid) {
        this.sid = sid;
    }
}
