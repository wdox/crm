package com.wzz.crm.query;

import com.wzz.crm.base.BaseQuery;

/**
 * 营销机会
 * 分页多条件 模板
 */
public class SaleChanceQuery extends BaseQuery {
    //分类查询用的
    private String customerName; //客户名称
    private String createMan;   //创建人
    private Integer state;      //分配状态

    //客户开发计划 用的
    private Integer devResult;  //开发状态
    private Integer assignMan;  //分配人

    public Integer getDevResult() {
        return devResult;
    }

    public void setDevResult(Integer devResult) {
        this.devResult = devResult;
    }

    public Integer getAssignMan() {
        return assignMan;
    }

    public void setAssignMan(Integer assignMan) {
        this.assignMan = assignMan;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCreateMan() {
        return createMan;
    }

    public void setCreateMan(String createMan) {
        this.createMan = createMan;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }
}
