package com.wzz.crm.query;

import com.wzz.crm.base.BaseQuery;

/**
 * 流失客户的多条件查询类
 */
public class CustomerLossQuery extends BaseQuery {

    //客户编号
    private String cusNo;

    //客户名称
    private String cusName;

    //流失状态
    private int state;

    public String getCusNo() {
        return cusNo;
    }

    public void setCusNo(String cusNo) {
        this.cusNo = cusNo;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }
}
