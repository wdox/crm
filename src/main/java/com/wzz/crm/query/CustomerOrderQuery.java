package com.wzz.crm.query;

import com.wzz.crm.base.BaseQuery;

/**
 * 客户订单的查询类
 */
public class CustomerOrderQuery extends BaseQuery {
    private Integer cusId;     //客户id

    public Integer getCusId() {
        return cusId;
    }

    public void setCusId(Integer cusId) {
        this.cusId = cusId;
    }
}
