package com.wzz.crm.query;

import com.wzz.crm.base.BaseQuery;

public class RoleQuery extends BaseQuery {

    //角色名称
    private String roleName;

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }
}
