package com.wzz.crm.query;

import com.wzz.crm.base.BaseQuery;

/**
 * 客户服务的查询条件类
 */
public class CustomerServeQuery extends BaseQuery {

    //状态类型
    private String state;

    //客户
    private String customer;

    //服务类型
    private String serveType;

    public String getServeType() {
        return serveType;
    }

    public void setServeType(String serveType) {
        this.serveType = serveType;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
