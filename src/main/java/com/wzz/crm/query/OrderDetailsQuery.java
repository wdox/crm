package com.wzz.crm.query;

import com.wzz.crm.base.BaseQuery;

/**
 * 订单详情多条件查询类
 */
public class OrderDetailsQuery extends BaseQuery {
    private Integer orderId;        //订单id

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }
}
