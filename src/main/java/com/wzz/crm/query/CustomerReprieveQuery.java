package com.wzz.crm.query;

import com.wzz.crm.base.BaseQuery;

/**
 * 暂缓流失客户的多条件查询类
 */
public class CustomerReprieveQuery extends BaseQuery {
    //流失客户id
    private String lossId;

    public String getLossId() {
        return lossId;
    }

    public void setLossId(String lossId) {
        this.lossId = lossId;
    }
}
