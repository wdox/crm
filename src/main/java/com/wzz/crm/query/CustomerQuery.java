package com.wzz.crm.query;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.wzz.crm.base.BaseQuery;
import org.springframework.format.annotation.DateTimeFormat;

import javax.xml.crypto.Data;
import java.util.Date;

/**
 * 客户查询类
 */
public class CustomerQuery extends BaseQuery {
    private String customerName;    //客户名称
    private String customerNo;  //客户编号
    private String level;   //客户级别

    //价格区间
    private Integer type; //1: 0-1000  2:1000-3000  ....

    //下单时间
    @JsonFormat(pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd") //2022-04-29
    private Date time;

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerNo() {
        return customerNo;
    }

    public void setCustomerNo(String customerNo) {
        this.customerNo = customerNo;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }
}
