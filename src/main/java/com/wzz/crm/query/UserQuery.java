package com.wzz.crm.query;

import com.wzz.crm.base.BaseQuery;

/**
 * 用户查询类
 * 用于用户类表的多条件查询
 */
public class UserQuery extends BaseQuery {

    //用户名
    String userName;

    //邮箱
    String email;

    //手机号
    String phone;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
