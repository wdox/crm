package com.wzz.crm.task;

import com.wzz.crm.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

//@Component
public class JobTask {

    @Autowired
    private CustomerService customerService;

    @Scheduled(cron = "0/5 * * * * ?")
    public void job(){
        customerService.updateCustomerLossState();
        System.out.println("更新成功!");
    }
}
