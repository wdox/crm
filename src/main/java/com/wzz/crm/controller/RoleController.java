package com.wzz.crm.controller;

import com.wzz.crm.base.BaseController;
import com.wzz.crm.base.ResultInfo;
import com.wzz.crm.domain.Role;
import com.wzz.crm.query.RoleQuery;
import com.wzz.crm.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * 角色控制器
 */
@RequestMapping("role")
@Controller
public class RoleController extends BaseController {

    @Autowired
    private  RoleService roleService;

    /**
     * 查询所有角色，id不为空时，查询对应用户的说有角色
     * @param userId    用户id
     */
    @RequestMapping("queryAllRoles")
    @ResponseBody
    public List<Map<String ,Object>> queryAllRoles(Integer userId){
        return  roleService.queryAllRoles(userId);
    }

    /**
     * 角色管理视图的转发
     * @return      角色管理的页面路径
     */
    @RequestMapping("index")
    public String index(){
        return "role/role";
    }

    /**
     * 条件分页查询
     * @param roleQuery     查询条件
     */
    @RequestMapping("list")
    @ResponseBody
    public Map<String ,Object> queryByParamsForTable(RoleQuery roleQuery){
        return roleService.queryByParamsForTable(roleQuery);
    }

    /**
     * 添加和修改视图的转发
     * @param roleId    要修改的角色id
     * @return          页面视图
     */
    @RequestMapping("toAddOrUpdateRolePage")
    public String toAddOrUpdateRolePage(Integer roleId, HttpServletRequest request){
        Role role = roleService.selectByPrimaryKey(roleId);
        request.setAttribute("role",role);
        return "role/add_update";
    }

    /**
     * 角色的添加
     * @param role  待添加的角色信息（包含角色名和助记名）
     */
    @RequestMapping("add")
    @ResponseBody
    public ResultInfo addRole(Role role){
        roleService.addRole(role);
        return success("角色添加成功！");
    }

    /**
     * 角色信息更新
     * @param role  待更新的角色信息
     */
    @RequestMapping("update")
    @ResponseBody
    public ResultInfo updateRole(Role role){
        roleService.updateRole(role);
        return success("角色信息更新成功！");
    }

    /**
     * 角色的删除
     * @param roleId    待删除的角色id
     */
    @RequestMapping("delete")
    @ResponseBody
    public ResultInfo deleteRole(Integer roleId){
       roleService.deleteByRoleId(roleId);
        return success("角色删除成功！");
    }

    /**
     * 角色的授权操作
     * @param mIds      模板id集合
     * @param roleId    角色id
     * @return          执行结果
     */
    @PostMapping("addGrant")
    @ResponseBody
    public ResultInfo addGrant(Integer[] mIds,Integer roleId ){
        roleService.addGrant(mIds,roleId);
        /*ResultInfo resultInfo = new ResultInfo();
        resultInfo.setCode(300);
        resultInfo.setMsg("授权失败！");
        return resultInfo;*/
        return success("授权成功！");
    }

}
