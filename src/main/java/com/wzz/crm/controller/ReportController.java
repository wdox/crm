package com.wzz.crm.controller;

import com.wzz.crm.base.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 统计报表的前端控制器
 */
@Controller
public class ReportController extends BaseController {

    @RequestMapping("report/{type}")
    public String index(@PathVariable Integer type){
        if (null != type) {
            //类型正确
            if (type==0){
                return "report/customer_contri";
            }else  if (type==1){
                return "report/customer_make";
            }else  if (type==3){
                return "report/customer_loss";
            }else  {
                return "";
            }
        }else{
            //路径不正确
            return "";
        }
    }
}

