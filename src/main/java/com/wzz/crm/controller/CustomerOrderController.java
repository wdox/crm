package com.wzz.crm.controller;

import com.wzz.crm.base.BaseController;
import com.wzz.crm.query.CustomerOrderQuery;
import com.wzz.crm.service.CustomerOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

/**
 * 客户订单控制器
 */
@Controller
@RequestMapping("order")
public class CustomerOrderController extends BaseController {

    @Autowired
    private CustomerOrderService customerOrderService;

    /**
     * 多条件分页查询
     * @param customerOrderQuery    条件
     * @return                      数据集合
     */
    @RequestMapping("list")
    @ResponseBody
    public Map<String,Object> queryList(CustomerOrderQuery customerOrderQuery){
        return customerOrderService.queryByParamsForTable(customerOrderQuery);
    }

    /**
     * 通过id查询订单信息
     * @param orderId   订单id
     * @return          （id,order_no,address,status，total）
     */
    @RequestMapping("toOrderDetailPage")
    public String toOrderDetailPage(Integer orderId, Model model){
        //查询订单信息
        Map<String,Object> map=customerOrderService.queryOrderAndTotalById(orderId);
        model.addAttribute("order",map);
        return "customer/customer_order_detail";
    }
}
