package com.wzz.crm.controller;


import com.wzz.crm.base.BaseController;
import com.wzz.crm.base.ResultInfo;

import com.wzz.crm.domain.User;
import com.wzz.crm.exceptions.ParamsException;
import com.wzz.crm.model.UserModel;
import com.wzz.crm.query.UserQuery;
import com.wzz.crm.service.UserService;
import com.wzz.crm.utils.LoginUserUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("user")
public class UserController extends BaseController {

    @Autowired
    private UserService service;

    /**
     1. 通过形参接收客户端传递的参数
     2. 调用业务逻辑层的登录方法，得到登录结果
     3. 响应数据给客户端
     * @param userName
     * @param userPwd
     */
    @PostMapping("login")
    @ResponseBody
    public ResultInfo login(String userName, String userPwd){
        //先定义个返回对象
        ResultInfo resultInfo = new ResultInfo();

        UserModel userModel = service.userLogin(userName, userPwd);
        //把用户数据放入结果result
        //现在有全局异常捕获
        resultInfo.setResult(userModel);
        return resultInfo;
    }


    /**
     1. 通过形参接收前端传递的参数 （原始密码、新密码、确认密码）
     2. 通过request对象，获取设置在cookie中的用户ID
     3. 调用Service层修改密码的功能，得到ResultInfo对象
     4. 返回ResultInfo对象
     * @param request
     * @param oldPassword
     * @param newPassword
     * @param repeatPassword
     * @return
     */
    @PostMapping("updatePwd")
    @ResponseBody
    public ResultInfo updateUserPassword(HttpServletRequest request,String oldPassword,
                                         String newPassword, String repeatPassword){
        ResultInfo resultInfo = new ResultInfo();
        //通过request对象，获取设置在cookie中的用户ID
        int userId = LoginUserUtil.releaseUserIdFromCookie(request);
        //调用Service层修改密码的功能，得到ResultInfo对象
        //全局异常捕获
        service.updateUserPassword(userId,oldPassword,newPassword,repeatPassword);
        resultInfo.setMsg("修改密码成功！");
        /*try {

        } catch (ParamsException e) {
            resultInfo.setCode(e.getCode());
            resultInfo.setMsg(e.getMsg());
            e.printStackTrace();
        }catch(Exception e){
            resultInfo.setCode(500);
            resultInfo.setMsg("修改密码失败！");
            e.printStackTrace();
        }*/
        return resultInfo;
    }
    //返回视图
    @RequestMapping("toPasswordPage")
    public String toPasswordPage(){
        if (true)throw new ParamsException();
        return "user/password";
    }

    /**
     * 获取全部的销售人员
     * @return  返回销售人员的list map（id ，name）集合
     */
    @GetMapping("queryAllSales")
    @ResponseBody
    public List<Map<String,Object>> queryAllSales(){
        List<Map<String, Object>> maps = service.queryAllSales();
        return maps;
    }

    /**
     * 用户管理主页面的转发
     * @return  用胡管理主页面的路径
     */
    @RequestMapping("index")
    public String index(){
        return "user/user";
    }

    /**
     * 用户列表展示，多条件查询
     * @param query 查询条件
     * @return  返回map集合
     */
    @RequestMapping("list")
    @ResponseBody
    public Map<String ,Object> queryByParamsForTable(UserQuery query){
        Map<String, Object> map = service.queryByParamsForTable(query);
        return map;
    }


    /**
     * 更新和添加视图的转发 更新时的数据回显
     * @param id        待更新数据的id（添加时没有）
     * @param model     存放待更新用户的信息
     */
    @RequestMapping("toAddOrUpdateUserPage")
    public String toAddOrUpdateUserPage(Integer id , Model model){
        if(null != id){
            model.addAttribute("userInfo",service.selectByPrimaryKey(id));
        }
        return "user/add_update";
    }

    /**
     * 用户添加
     * @param user  添加用户的信息
     */
    @RequestMapping("add")
    @ResponseBody
    public ResultInfo saveUser(User user){
        service.saveUser(user);
        return success("用户添加成功！");
    }

    /**
     * 用户添加
     * @param user  更新用户的信息
     */
    @RequestMapping("update")
    @ResponseBody
    public ResultInfo updateUser(User user){
        service.updateUser(user);
        return success("用户信息修改成功！");
    }

    @RequestMapping("delete")
    @ResponseBody
    public ResultInfo deleteUser(Integer [] ids){
        service.deleteBatchById(ids);
        return success("用户删除成功！");
    }

    /**
     * 获取全部的客户经理
     * @return  返回客户经理的list map（id ，name）集合
     */
    @GetMapping("queryAllCustomerManagers")
    @ResponseBody
    public List<Map<String,Object>> queryAllCustomerManagers(){
        List<Map<String, Object>> maps = service.queryAllCustomerManagers();
        return maps;
    }
}
