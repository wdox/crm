package com.wzz.crm.controller;

import com.wzz.crm.base.BaseController;
import com.wzz.crm.base.ResultInfo;
import com.wzz.crm.domain.CustomerLoss;
import com.wzz.crm.query.CustomerLossQuery;
import com.wzz.crm.service.CustomerLossService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

/**
 * 客户流失信息前端控制器
 */
@Controller
@RequestMapping("customer_loss")
public class CustomerLossController extends BaseController {

    @Autowired
    private CustomerLossService customerLossService;

    /**
     * 客户流失管理主页面打开
     * @return          客户流失视图名称
     */
    @RequestMapping("index")
    public String index(){
        return "customerLoss/customer_loss";
    }

    /**
     * 流失客户的多条件分页查询
     * @param customerLossQuery     查询条件
     * @return                      流失客户集合
     */
    @RequestMapping("list")
    @ResponseBody
    public Map<String,Object> queryList(CustomerLossQuery customerLossQuery){
        return  customerLossService.queryByParamsForTable(customerLossQuery);
    }

    /**
     * 详情或者暂缓流失页面的打开以及数据回显
     * @param lossId    流失客户id
     */
    @RequestMapping("toCustomerLossPage")
    public String toCustomerLossPage(Integer lossId, Model model){
        //通过流失客户id查询流失客户信息
        CustomerLoss customerLoss = customerLossService.selectByPrimaryKey(lossId);
        model.addAttribute("customerLoss", customerLoss);
        return "customerLoss/customer_rep";
    }

    /**
     * 确认流失
     * @param id            确认流失的客户的id
     * @param lossReason    流失原因
     * @return
     */
    @RequestMapping("updateCustomerLossStateById")
    @ResponseBody
    public ResultInfo updateCustomerLossStateById(Integer id,String lossReason){
         customerLossService.updateCustomerLossStateById(id,lossReason);
         return success("确认流失成功！");
    }
}
