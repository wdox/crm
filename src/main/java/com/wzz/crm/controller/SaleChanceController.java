package com.wzz.crm.controller;

import com.wzz.crm.annoation.RequiredPermission;
import com.wzz.crm.base.BaseController;
import com.wzz.crm.base.ResultInfo;
import com.wzz.crm.domain.SaleChance;
import com.wzz.crm.domain.User;
import com.wzz.crm.query.SaleChanceQuery;
import com.wzz.crm.service.SaleChanceService;
import com.wzz.crm.utils.CookieUtil;
import com.wzz.crm.utils.LoginUserUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.util.List;
import java.util.Map;

/**
 * 营销机会控制器
 */
@Controller
@RequestMapping("sale_chance")
public class SaleChanceController extends BaseController {

    @Autowired
    private SaleChanceService service;


    /**
     * 分页显示营销机会，多条件查询也用到这个
     *
     * @param query 查询类 父类（当前页，每页条数）+继承（条件）
     */
    @RequiredPermission(code = "101001")
    @ResponseBody
    @RequestMapping("list")
    public Map<String, Object> querySaleChanceByParams(SaleChanceQuery query
            , Integer flag, HttpServletRequest request) {
        //判断flag，值为1表示为客户开发计划获取数据列表
        if (null != flag && flag == 1) {
            //获取当前用户的id
            int id = LoginUserUtil.releaseUserIdFromCookie(request);
            //只查询本用户的开发计划
            query.setAssignMan(id);
        }
        return service.querySaleChanceByParams(query);
    }

    /**
     * 删除营销关系
     *
     * @param ids 删除的id数组
     */
    @RequiredPermission(code = "101003")
    @PostMapping("delete")
    @ResponseBody
    public ResultInfo delete(Integer[] ids) {
        service.deleteSaleChance(ids);
        return success("删除成功！");
    }

    /**
     * 打开营销机会的页面
     */
    @RequiredPermission(code = "1010")
    @RequestMapping("index")
    public String index() {
        return "saleChance/sale_chance";
    }

    /**
     * 添加更新营销机会视图的转发
     */
    @RequestMapping("toSaleChancePage")
    public String toSaleChancePage(Integer saleChanceId, Model model) {
        //判断id是否为空，不为空表示为更新操作，需要返回当前营销机会的数据
        if (null != saleChanceId) {
            //通过id查询营销机会的数据
            SaleChance saleChance = service.selectByPrimaryKey(saleChanceId);
            model.addAttribute("saleChance", saleChance);
        }
        return "saleChance/add_update";
    }


    /**
     * 营销机会更新
     */
    @RequiredPermission(code = "101004")
    @PostMapping("update")
    @ResponseBody
    public ResultInfo updateSaleChance(SaleChance saleChance) {
        service.updateSaleChance(saleChance);
        return success("更新成功！");
    }

    /**
     * 添加新的营销关系
     *
     * @param saleChance 营销关系对象
     * @return
     */
    @RequiredPermission(code = "101002")
    @RequestMapping("add")
    @ResponseBody
    public ResultInfo add(SaleChance saleChance, HttpServletRequest request) {
        ResultInfo resultInfo = new ResultInfo();
        //在cookie中获取用户名（创建人）
        String userName = CookieUtil.getCookieValue(request, "userName");
        saleChance.setCreateMan(userName);
        //添加营销机会
        service.addSaleChance(saleChance);
        return success("营销机会添加成功！");
    }

    /**
     * 更新营销机会的开发状态
     *
     * @param id        营销机会的id
     * @param devResult 开发状态
     */
    @ResponseBody
    @RequestMapping("updateSaleChanceDevResult")
    public ResultInfo updateSaleChanceDevResult(Integer id, Integer devResult) {
        service.updateSaleChanceDevResult(id, devResult);

        try {
            Class<?> aClass = Class.forName("com.wzz.crm.domain.User");

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return success("更改成功！");
    }

}