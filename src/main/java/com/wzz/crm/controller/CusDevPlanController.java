package com.wzz.crm.controller;

import com.wzz.crm.base.BaseController;
import com.wzz.crm.base.ResultInfo;
import com.wzz.crm.domain.CusDevPlan;
import com.wzz.crm.domain.SaleChance;
import com.wzz.crm.query.CusDevPlanQuery;
import com.wzz.crm.service.CusDevPlanService;
import com.wzz.crm.service.SaleChanceService;
import com.wzz.crm.utils.AssertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * 客户开发计划控制器
 */
@Controller
@RequestMapping("cus_dev_plan")
public class CusDevPlanController extends BaseController {

    /**
     * git分支合并
     * 当前必须切换到master主分支
     * 然后在要合并的分支上点击merge
     */
    @Autowired
    private SaleChanceService saleChanceService;

    @Autowired
    private CusDevPlanService cusDevPlanService;

    /**
     * 客户开发计划主页面视图的转发
     * @return  客户开发计划主页面
     */
    @RequestMapping("index")
    public String index(){
        return "cusDevPlan/cus_dev_plan";
    }

    /**
     * 列表的行工具栏的详情与开发页面的转发
     * 对应id的开发计划的数据回显
     */
    @RequestMapping("toCusDevPlanPage")
    public String toCusDevPlanPage(Integer id, Model model){
        //查询相应id的营销开发计划
        SaleChance saleChance = saleChanceService.selectByPrimaryKey(id);
        model.addAttribute("saleChance",saleChance);
        return "cusDevPlan/cus_dev_plan_data";
    }


    /**
     * 多条件查询计划项列表
     */
    @RequestMapping("list")
    @ResponseBody
    public Map<String ,Object> queryCusDevPlanByParams(CusDevPlanQuery query){

        AssertUtil.isTrue(null == query.getSid() , "营销计划不存在！");
        Map<String, Object> map = cusDevPlanService.queryCusDevPlansByParams(query);
        return map;
    }

    /**
     * 计划项 添加
     */
    @PostMapping("add")
    @ResponseBody
    public ResultInfo saveCusDevPlan(CusDevPlan cusDevPlan){
        cusDevPlanService.saveCusDevPlan(cusDevPlan);
        return success("计划项添加成功！");
    }

    /**
     * 添加 和 更新 页面的转发
     * 数据id存在是表示更新，要做一个数据回显
     */
    @RequestMapping("toAddOrUpdateCusDevPlanPage")
    public String toAddOrUpdateCusDevPlanPage(Integer sId,Integer id, HttpServletRequest request){
        request.setAttribute("sid",sId);
        if (null != id){
            CusDevPlan cusDevPlan = cusDevPlanService.selectByPrimaryKey(id);
            request.setAttribute("cusDevPlan",cusDevPlan);
        }
        return "cusDevPlan/add_update";
    }

    /**
     * 计划项 更新
     */
    @PostMapping("update")
    @ResponseBody
    public ResultInfo updateCusDevPlan(CusDevPlan cusDevPlan){
        cusDevPlanService.updateCusDevPlan(cusDevPlan);
        return success("计划项修改成功！");
    }

    /**
     * 删除开发计划
     * @param id    开发计划id
     */
    @RequestMapping("delete")
    @ResponseBody
    public ResultInfo deleteCusDevPlan(Integer id){
        cusDevPlanService.deleteCusDevPlan(id);
        return success("开发计划删除成功！");
    }

}
