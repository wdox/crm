package com.wzz.crm.controller;

import com.baomidou.mybatisplus.extension.api.R;
import com.wzz.crm.base.BaseController;
import com.wzz.crm.base.ResultInfo;
import com.wzz.crm.domain.CustomerReprieve;
import com.wzz.crm.query.CustomerReprieveQuery;
import com.wzz.crm.service.CustomerReprieveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

/**
 * 暂缓流失客户前端控制器
 */
@RequestMapping("customer_rep")
@Controller
public class CustomerReprieveController extends BaseController {

    @Autowired
    private CustomerReprieveService customerReprieveService;

    /**
     * 查询全部暂缓流失客户
     * @param customerReprieveQuery     多条件分页查询
     */
    @ResponseBody
    @RequestMapping("list")
    public Map<String, Object> queryLis(CustomerReprieveQuery customerReprieveQuery) {
        return customerReprieveService.queryByParamsForTable(customerReprieveQuery);
    }

    /**
     * 添加和修改暂缓的视图打开
     * @param lossId        暂缓客户id
     * @param id            暂缓记录的id（查询暂缓，用于修改的数据回显）
     */
    @RequestMapping("toAddOrUpdateCustomerReprPage")
    public String toAddOrUpdateCustomerReprPage(Integer lossId, Integer id,Model model){
        //通过流失客户id查询当前流失客户的信息
        CustomerReprieve customerRep = customerReprieveService.selectByPrimaryKey(id);
        //将数据存入数据模型
        model.addAttribute("lossId", lossId);
        model.addAttribute("customerRep", customerRep);
        return "customerLoss/customer_rep_add_update";
    }

    @RequestMapping("add")
    @ResponseBody
    public ResultInfo addCustomerReprieve(CustomerReprieve customerReprieve){
        customerReprieveService.addCustomerReprieve(customerReprieve);
        return success("添加成功！");
    }

    @RequestMapping("update")
    @ResponseBody
    public ResultInfo updateCustomerReprieve(CustomerReprieve customerReprieve){
        customerReprieveService.updateCustomerReprieve(customerReprieve);
        return success("修改成功！");
    }

    @RequestMapping("delete")
    @ResponseBody
    public ResultInfo deleteCustomerReprieve(Integer id){
        customerReprieveService.deleteById(id);
        return success("暂缓措施删除成功！");
    }
}
