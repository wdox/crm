package com.wzz.crm.controller;

import com.wzz.crm.base.BaseController;
import com.wzz.crm.base.ResultInfo;
import com.wzz.crm.domain.Module;
import com.wzz.crm.model.TreeModel;
import com.wzz.crm.service.ModuleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;


import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * 模块（权限）控制器
 */
@Controller
@RequestMapping("module")
public class ModuleController extends BaseController {

    @Autowired
    private ModuleService moduleService;

    /**
     * 授权创库页面视图的转发
     * @param roleId    角色id
     */
    @RequestMapping("toAddGrantPage")
    public String toAddGrantPage(Integer roleId ,HttpServletRequest request){
        request.setAttribute("roleId",roleId);
        return "role/grant";
    }

    /**
     * 查询所有的模块集合  用于授权的zTree使用的数据给
     * @param roleId    角色id 用于当前角色已经选择的权限回显
     */
    @GetMapping("queryAllModules")
    @ResponseBody
    public List<TreeModel> queryAllModules(Integer roleId){
        return moduleService.queryAllModules(roleId);
    }

    /**
     * 菜单管理（模块）视图的转发
     */
    @RequestMapping("index")
    public String index(){
        return "module/module";
    }

    /**
     * 查询所有的菜单（模块）列表，包含记录的全部信息
     */
    @RequestMapping("list")
    @ResponseBody
    public Map<String,Object> queryModuleList(){
        return moduleService.queryModuleList();
    }

    /**
     * 模块资源的添加
     * @param module    模块
     */
    @RequestMapping("add")
    @ResponseBody
    public ResultInfo addModule(Module module){
        moduleService.addModule(module);
        return success("添加成功！");
    }

    /**
     * 子模块的添加页面转发
     */
    @RequestMapping("toAddModulePage")
    public String toAddModulePage(Integer grade,Integer parentId,HttpServletRequest request){
        request.setAttribute("grade", grade);
        request.setAttribute("parentId", parentId);
        return "module/add";
    }

    /**
     * 修改资源
     */
    @PostMapping("update")
    @ResponseBody
    public ResultInfo updateModule(Module module) {
        moduleService.updateModule(module);
        return success("更新成功！");
    }

    /**
     * 修改页面视图的转发
     * @param id        要修改模块的id
     */
    @RequestMapping("toUpdateModulePage")
    public String toUpdateModulePage(Integer id,HttpServletRequest request){
        Module module = moduleService.selectByPrimaryKey(id);
        request.setAttribute("module", module);
        return "module/update";
    }

    /**
     * 删除module
     * @param id    待删除的id
     */
    @RequestMapping("delete")
    @ResponseBody
    public ResultInfo deleteModule(Integer id){
        moduleService.deleteModule(id);
        return success("删除成功！");
    }
}
