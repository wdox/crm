package com.wzz.crm.controller;

import com.wzz.crm.base.BaseController;
import com.wzz.crm.query.OrderDetailsQuery;
import com.wzz.crm.service.OrderDetailsService;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

/**
 * 订单详情前端控制器
 */
@Controller
@RequestMapping("order_details")
public class OrderDetailsController extends BaseController {

    @Autowired
    private OrderDetailsService orderDetailsService;

    /**
     * 多条件分页查询
     * @param orderDetailsQuery   订单id
     * @return                    订单详情列表
     */
    @RequestMapping("list")
    @ResponseBody
    public Map<String,Object> queryList(OrderDetailsQuery orderDetailsQuery){
        return orderDetailsService.queryByParamsForTable(orderDetailsQuery);
    }

}
