package com.wzz.crm.controller;

import com.wzz.crm.base.BaseController;
import com.wzz.crm.base.ResultInfo;
import com.wzz.crm.domain.CustomerServe;
import com.wzz.crm.query.CustomerServeQuery;
import com.wzz.crm.service.CustomerServeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

/**
 * 客户服务前端控制器
 */
@Controller
@RequestMapping("customer_serve")
public class CustomerServeController extends BaseController {

    @Autowired
    private CustomerServeService customerServeService;

    /**
     * 服务管理五个页面的跳转
     * @param type      页面的对应的类型值（页面的标识）
     */
    @RequestMapping("index/{type}")
    public String index(@PathVariable Integer type){
        if (null != type) {
            if (type == 1){
                return "customerServe/customer_serve";
            }else if (type == 2){
                return "customerServe/customer_serve_assign";
            }else if (type == 3){
                return "customerServe/customer_serve_proce";
            }else if (type == 4){
                return "customerServe/customer_serve_feed_back";
            }else if (type == 5){
                return "customerServe/customer_serve_archive";
            }else{
                return "";
            }
        }else{
            return "";
        }
    }

    /**
     * 客户服务列表的查询
     * @param customerServeQuery        查询条件
     * @return                          返回集合
     */
    @RequestMapping("list")
    @ResponseBody
    public Map<String ,Object> queryList(CustomerServeQuery customerServeQuery){
        return customerServeService.queryByParamsForTable(customerServeQuery);
    }

    /**
     * 打开客户服务添加页面
     * @return      添加页面的视图名称
     */
    @RequestMapping("toAddCustomerServePage")
    public String toAddCustomerServePage(){
        return "customerServe/customer_serve_add";
    }

    /**
     * 添加服务
     * @param customerServe     待添加的客户服务
     */
    @RequestMapping("add")
    @ResponseBody
    public ResultInfo addCustomerServe(CustomerServe customerServe){
        customerServeService.addCustomerServe(customerServe);
        return success("添加成功！");
    }

    /**
     * 服务更新操作
     *  分配 处理  反馈
     */
    @ResponseBody
    @RequestMapping("update")
    public ResultInfo updateCustomerServe(CustomerServe customerServe){
        customerServeService.updateCustomerServe(customerServe);
        return success("操作成功！");
    }

    @RequestMapping("toCustomerServeAssignPage")
    public String toCustomerServeAssignPage(Integer id, Model model){
        CustomerServe customerServe = customerServeService.selectByPrimaryKey(id);
        model.addAttribute("customerServe", customerServe);
        return "customerServe/customer_serve_assign_add";
    }

    @RequestMapping("toCustomerServeProcePage")
    public String toCustomerServeProcePage(Integer id, Model model){
        CustomerServe customerServe = customerServeService.selectByPrimaryKey(id);
        model.addAttribute("customerServe", customerServe);
        return "customerServe/customer_serve_proce_add";
    }

    @RequestMapping("toCustomerServeFeedBackPage")
    public String totoCustomerServeFeedBackPage(Integer id, Model model){
        CustomerServe customerServe = customerServeService.selectByPrimaryKey(id);
        model.addAttribute("customerServe", customerServe);
        return "customerServe/customer_serve_feed_back_add";
    }

}
