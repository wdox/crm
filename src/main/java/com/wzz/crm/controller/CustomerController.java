package com.wzz.crm.controller;

import com.wzz.crm.base.BaseController;
import com.wzz.crm.base.ResultInfo;
import com.wzz.crm.domain.Customer;
import com.wzz.crm.query.CustomerQuery;
import com.wzz.crm.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

/**
 * 顾客有关前端控制器
 */
@Controller
@RequestMapping("customer")
public class CustomerController extends BaseController {
    @Autowired
    private CustomerService customerService;

    /**
     * 客户信息管理的视图的跳转
     * @return  资源路径
     */
    @RequestMapping("index")
    public String index(){
        return "customer/customer";
    }

    /**
     * 客户信息的多条件分页查询
     * @param customerQuery     条件查询条件
     * @return                  满足layUi表格格式的数据
     */
    @RequestMapping("list")
    @ResponseBody
    public Map<String,Object> queryList(CustomerQuery customerQuery){
        //BaseService里的方法
        return customerService.queryByParamsForTable(customerQuery);
    }

    /**
     * 添加和修改视图的跳转，修改时数据的回显
     * @param id    要修的客户的id
     * @return  跳转的视图名称
     */
    @RequestMapping("toAddOrUpdateCustomerPage")
    public String toAddOrUpdateCustomerPage(Integer id,Model model){

        /*返回ModelAndView
        Model就是request域中的数据
        View就是要跳转的视图或者其他处理器方法
        ModelAndView modelAndView = new ModelAndView();
        Customer customer = customerService.selectByPrimaryKey(id);
        modelAndView.addObject("customer", customer);
        modelAndView.setViewName("customer/add_update");
        return modelAndView;*/

        //直接返回视图名称，并设置request域中的数据
        /*Customer customer = customerService.selectByPrimaryKey(id);
        request.setAttribute("customer", customer);
        return "customer/add_update";*/

        Customer customer = customerService.selectByPrimaryKey(id);
        model.addAttribute("customer", customer);
        return "customer/add_update";
    }

    /**
     * 用户的添加
     * @param customer  待添加的用户
     * @return          执行结果
     */
    @RequestMapping("add")
    @ResponseBody
    public ResultInfo addCustomer(Customer customer){
        customerService.saveCustomer(customer);
        return success("添加成功！");
    }

    /**
     * 用户的修改
     * @param customer  待添加的用户
     * @return          执行结果
     */
    @RequestMapping("update")
    @ResponseBody
    public ResultInfo updateCustomer(Customer customer){
        customerService.updateCustomer(customer);
        return success("修改成功！");
    }

    /**
     * 删除客户信息
     * @param id    待删除的客户id
     * @return      返回执行结果
     */
    @RequestMapping("delete")
    @ResponseBody
    public ResultInfo deleteCustomer(Integer id){
        customerService.deleteCustomer(id);
        return success("删除成功！");
    }

    /**
     * 打开客户订单页面
     * @param customerId    客户id
     */
    @RequestMapping("toCustomerOrderPage")
    public String toCustomerOrderPage(Integer customerId,Model model){
        //通过用户id查询客户的基本信息
        Customer customer = customerService.selectByPrimaryKey(customerId);
        model.addAttribute("customer",customer);
        return "customer/customer_order";
    }

    /**
     * 多条件分页查询查询贡献值 客户列表
     * @param customerQuery     查询条件
     */
    @RequestMapping("queryCustomerContributionByParams")
    @ResponseBody
    public Map<String,Object> queryCustomerContributionByParams(CustomerQuery customerQuery){
        return customerService.queryCustomerContributionByParams(customerQuery);
    }

    @RequestMapping("countCustomerMake")
    @ResponseBody
    public Map<String,Object> countCustomerMake(){
        return customerService.countCustomerMake();
    }

    @RequestMapping("countCustomerMake02")
    @ResponseBody
    public Map<String,Object> countCustomerMake02(){
        return customerService.countCustomerMake02();
    }

}
