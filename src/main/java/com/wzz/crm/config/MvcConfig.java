package com.wzz.crm.config;

import com.wzz.crm.interceptors.NoLoginInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;


/**
 * 配置类
 */
@Configuration
public class MvcConfig implements WebMvcConfigurer {

    //将方法返回值交给ioc容器维护
    @Autowired
    private NoLoginInterceptor noLoginInterceptor;
   /* @Bean
    public NoLoginInterceptor noLoginInterceptor(){
        return new NoLoginInterceptor();
    }*/

    /**
     * 添加拦截器
     * @param registry
     */

    public void addInterceptors(InterceptorRegistry registry){

        //需要一个实现HandlerInterceptor接口的拦截器实例，这里使用的是NoLoginInterceptor
        registry.addInterceptor(noLoginInterceptor)
                .addPathPatterns("/**")
                .excludePathPatterns("/index","/user/login","/css/**","/image/**","/js/**","/lib/**");
    }

}
