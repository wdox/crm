package com.wzz.crm.interceptors;

import com.wzz.crm.exceptions.NoLoginException;
import com.wzz.crm.service.UserService;
import com.wzz.crm.utils.LoginUserUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 信息检查拦截器
 */
@Component
public class NoLoginInterceptor implements HandlerInterceptor {

    @Autowired
    private UserService userService;

    /**
     * 判断用户是否是登陆状态
     *  获取cookie对象，解析用户id的值
     *      如果用户id不为空，且在数据库中用对应id的数据，表示用户存在
     *      否则，请求不合法，没有登陆信息，进行拦截重定向到登陆界面。
     * @param request
     * @param response
     * @param handler
     * @return
     * @throws Exception
     */

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        //获取cookie中的用户id
        Integer id = LoginUserUtil.releaseUserIdFromCookie(request);
        //判断是有用户id并且id合法
        if (null == id || null == userService.selectByPrimaryKey(id)){
            //登录信息异常，重定向到登陆界面，在这里抛出用户登录异常
            throw new NoLoginException();
        }
        return true;
    }
}
