package com.wzz.crm.aspect;

import com.wzz.crm.annoation.RequiredPermission;
import com.wzz.crm.dao.PermissionMapper;
import com.wzz.crm.exceptions.AuthException;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import javax.servlet.http.HttpSession;
import java.util.List;


/**
 * 权限切面类
 */

@Aspect // 定义切面类
@Component //声明组件
public class PermissionAspect {
    //@Autowired
    @Autowired
    private HttpSession session;

    @Around("@annotation(com.wzz.crm.annoation.RequiredPermission)")
    public Object around(ProceedingJoinPoint pjp) throws Throwable {
        Object result = null;
        //在session中获取permissions,拿到当前用户所拥有的权限
        List<String> permissions = (List<String>) session.getAttribute("permissions");
        //先判断是否有权限，即permissions是否为null，或者长度 <1
        if (null == permissions || permissions.size()<1){
            //抛出权限异常
            throw new AuthException();
        }
        //有权限就判断是否有当前操作的权限
        //获取封装了署名信息的对象,在该对象中可以获取到目标方法名,所属类的Class等信息
        //获取被增强的方法相关信息.其后续方法有两个
        //getDeclaringTypeName: 返回方法所在的包名和类名
        //getName() : 返回方法名
        MethodSignature signature = (MethodSignature) pjp.getSignature();
        //的到方法上的注解,获取指定注释类型的注释。该方法以对象的形式返回该类
        RequiredPermission requiredPermission = signature.getMethod().getAnnotation(RequiredPermission.class);
        //判断注解上的权限码
        //Integer.parseInt(requiredPermission.code())
        if (!(permissions.contains(requiredPermission.code()))){
            //用户的权限码里没有此方法所需要的权限码，抛出权限异常
            System.out.println(requiredPermission.code());
            throw new AuthException();
        }
        result = pjp.proceed();
        return  result;
    }


}
