package com.wzz.crm.service;

import com.wzz.crm.base.BaseService;
import com.wzz.crm.dao.ModuleMapper;
import com.wzz.crm.dao.PermissionMapper;
import com.wzz.crm.domain.Module;
import com.wzz.crm.model.TreeModel;
import com.wzz.crm.utils.AssertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 模块服务类
 */
@Service
public class ModuleService extends BaseService<Module, Integer> {

    @Autowired
    private ModuleMapper moduleMapper;

    @Autowired
    private PermissionMapper permissionMapper;

    /**
     * 查询全部的模块，并根据角色id选中当前角色已经拥有的权限
     *
     * @param roleId 角色id
     * @return 全部权限id(当前角色的checked = true)
     */
    public List<TreeModel> queryAllModules(Integer roleId) {
        //查询说有的模块资源
        List<TreeModel> treeModuleList = moduleMapper.queryAllModules(roleId);
        //查询当前角色拥有的权限
        List<Integer> permissionIds = permissionMapper.selectModuleIdsByRoleId(roleId);

        //判断，给拥有的权限模块设置checked设置true
        if (!permissionIds.isEmpty()) {
            //权限不为空，遍历模块资源
            treeModuleList.forEach(treeModel -> {
                if (permissionIds.contains(treeModel.getId())) {
                    treeModel.setChecked(true);
                }
            });
        }
        return treeModuleList;
    }

    //查询所有的module集合
    public Map<String, Object> queryModuleList() {
        Map<String, Object> map = new HashMap<>();
        /*"code": 0,
          "msg": "",
          "count": 19,
          "data": []+
        */
        List<Module> lists = moduleMapper.queryModuleList();
        map.put("code", 0);
        map.put("msg", "菜单列表查询成功！");
        map.put("count", lists.size());
        map.put("data", lists);
        return map;
    }

    /**
     * 添加资源
     * 1.参数校验
     *  层级 grade
     *      非空 ，必为 0 ，1， 2
     *           模块名称 moduleName
     *            非空，且同一层级下模块名称唯一
     *      地址 url
     *          二级菜单（grade = 1 ），非空且唯一
     *      父级菜单 parentId
     *          一级菜单 （grade = 0） null
     *          二级或者三级菜单 （grand = 1 | 2 ），飞非空，父级菜单一定存在
     *      权限码 optValue
     *          非空且唯一
     * 2.设置参数默认值
     *      有效值 isValid 1
     *      创建时间 createDate 系统当前时间
     *      更新时间 updateDate 系统当前时间
     * 3.执行添加操作， 判断受影响行数
     *
     * @param module 模板资源
     */
    @Transactional(propagation = Propagation.REQUIRED)
    public void addModule(Module module) {
        //1.参数校验
        //层级 grade 非空 ，必为 0 ，1， 2
        Integer grade = module.getGrade();
        AssertUtil.isTrue(null == grade || !(grade == 0 || grade == 1 || grade == 2), "层级信息不能为空！");
        //模块名称 moduleName 非空，且同一层级下模块名称唯一
        AssertUtil.isTrue(null == module.getModuleName(), "模块名称不能为空！");
        AssertUtil.isTrue(null != moduleMapper.queryModuleByGradeAndModuleName(grade, module.getModuleName()), "模块名称已经存在！");
        //地址 url 二级菜单（grade = 1 ），非空且唯一
        if (grade == 1) {
            //判断存在
            AssertUtil.isTrue(null == module.getUrl(), "url路径不能为空！");
            //判断唯一
            AssertUtil.isTrue(null != moduleMapper.queryModuleByGradeAndUrl(grade, module.getUrl()), "url路径已经存在！");
        }
        //父级菜单 parentId
        if (grade == 0) {
            // 一级菜单 （grade = 0） -1
            module.setParentId(-1);
        } else if (grade == 1 || grade == 2) {
            // 二级或者三级菜单 （grand = 1 | 2 ），飞非空，父级菜单一定存在
            AssertUtil.isTrue(null == module.getParentId(), "父级菜单不能为空！");
            AssertUtil.isTrue(null == selectByPrimaryKey(module.getParentId()), "父级菜单不存在！");
        }
        //权限码 optValue 非空且唯一
        AssertUtil.isTrue(null == module.getOptValue(), "权限码不能为空！");
        AssertUtil.isTrue(null != moduleMapper.queryModuleByOptValue(module.getOptValue()), "权限码已经存在！");
        //2.设置参数默认值
        //有效值 isValid 1
        module.setIsValid((byte) 1);
        // 创建时间 createDate 系统当前时间
        module.setCreateDate(new Date());
        // 更新时间 updateDate 系统当前时间
        module.setUpdateDate(new Date());
        //3.执行添加操作
        AssertUtil.isTrue(moduleMapper.insertSelective(module) != 1, "添加失败！");
    }

    /**
     * 修改资源
     *  1.参数校验
     *      id 非空 且存在
     *      层级  非空 =（0 1 2）
     *      模块名称 非空 统计目录下模块名称唯一 （排除本身）
     *      url 二级菜单非空，且你能重复（排除本身）
     *      权限码 非空且不可重复（排除本身）
     *  2.设置默认值
     *      修改时间  当前系统时间
     *  3.执行更新操作，判断受影响的行数
     * @param module    修改的的模板资源
     */
    @Transactional(propagation = Propagation.REQUIRED)
    public void updateModule(Module module) {
        //1.参数校验
        //id 非空 且存在
        AssertUtil.isTrue(null == module.getId(), "资源ID为空！");
        Module tempModule = moduleMapper.selectByPrimaryKey(module.getId());
        AssertUtil.isTrue(null == tempModule, "资源不存在！");
        //层级  非空 =（0 1 2）
        Integer grade = module.getGrade();
        AssertUtil.isTrue(null == grade || !(grade == 0 || grade == 1 || grade == 2), "资源层级不能为空！");
        //模块名称 非空 统计目录下模块名称唯一 （排除本身）
        AssertUtil.isTrue(null == module.getModuleName(), "资源名称不能为空！");
        //通过层级和名称去查询模块
        tempModule = moduleMapper.queryModuleByGradeAndModuleName(grade, module.getModuleName());
        if (null != tempModule) {
            //不为空，判断是否是本身
            AssertUtil.isTrue(!module.getId().equals(tempModule.getId()),"该层级菜单名称已经存在！");
        }
        //url 二级菜单非空，且你能重复（排除本身）
        if (grade == 1) {
            AssertUtil.isTrue(null == module.getUrl(), "url不能为空！");
            //通过层级和url查询module
            tempModule = moduleMapper.queryModuleByGradeAndUrl(grade, module.getUrl());
            if (null != tempModule) {
                //不为空，则判断是否是本身
                AssertUtil.isTrue(!module.getId().equals(tempModule.getId()),"url已经存在！");
            }
        }
        //权限码 非空且不可重复（排除本身）
        AssertUtil.isTrue(null == module.getOptValue(), "权限码不能为空！");
        //通过权限码查询查询module
        tempModule=moduleMapper.queryModuleByOptValue(module.getOptValue());
        if (null != tempModule) {
            //不为空，则判断是否是本身
            AssertUtil.isTrue(!module.getId().equals(tempModule.getId()),"权限码已经存在！");
        }
        //2.修改默认参数
        //修改时间  当前系统时间
        module.setUpdateDate(new Date());
        tempModule=null;
        //3.执行更新操作，判断返回结果
        AssertUtil.isTrue(moduleMapper.updateByPrimaryKeySelective(module) != 1, "更新失败！");
    }

    /**
     * 删除module（逻辑删除，实际修改有效值）
     * 1.判断是否可以删除
     *      判断是id否为空，是否存在
     *      判断是否右子记录 （子菜单），若有删除失败
     *      判断是否绑定的权限，有则删除权限
     * 2.设置默认值
     *      有效值  0
     *      修改时间 当前时间
     * 3.执行更新操作，判断返回值
     * @param id    要删除的模块id
     */
    public void deleteModule(Integer id){
        //1.校验是否符合删除
        //判断是否为空，是否存在
        AssertUtil.isTrue(null == id, "带删除菜单资源不存在（id为空）！");
        AssertUtil.isTrue(null == moduleMapper.selectByPrimaryKey(id), "带删除菜单资源不存在!");
        //判断是否右子记录 （子菜单），若有删除失败
        //通过父菜单查询module（把改id传入）
        AssertUtil.isTrue(null != moduleMapper.queryModuleByParentId(id), "该资源存在子关联资源，不能删除！");
        //判断是否绑定的权限，有则删除权限（通过moduleId查询权限表）
        Integer count = permissionMapper.countPermissionByModuleId(id);
        if (count > 0) {
            //存在的权限绑定存在，全部删除
            AssertUtil.isTrue(permissionMapper.deleteByModuleId(id) != count ,"更新失败（权限解绑失败）！");
        }
        //2.设置默认参数
        Module module = new Module();
        module.setId(id);
        module.setIsValid((byte) 0);
        module.setUpdateDate(new Date());
        //3.执行更新操作，判断返回
        AssertUtil.isTrue(moduleMapper.updateByPrimaryKeySelective(module) != 1, "删除失败！");
    }
}
