package com.wzz.crm.service;

import com.wzz.crm.base.BaseService;
import com.wzz.crm.dao.UserMapper;
import com.wzz.crm.dao.UserRoleMapper;
import com.wzz.crm.domain.User;
import com.wzz.crm.domain.UserRole;
import com.wzz.crm.model.UserModel;
import com.wzz.crm.utils.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class UserService extends BaseService<User,Integer> {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private UserRoleMapper userRoleMapper;

    /**
     * 用户登录服务
     1. 参数判断，判断用户姓名、用户密码非空弄
        如果参数为空，抛出异常（异常被控制层捕获并处理）
     2. 调用数据访问层，通过用户名查询用户记录，返回用户对象
     3. 判断用户对象是否为空
        如果对象为空，抛出异常（异常被控制层捕获并处理）
     4. 判断密码是否正确，比较客户端传递的用户密码与数据库中查询的用户对象中的用户密码
        如果密码不相等，抛出异常（异常被控制层捕获并处理）
     5. 如果密码正确，登录成功
     * @param userName
     * @param userPassword
     */
    public UserModel userLogin(String userName,String userPassword){
        //1. 参数判断，判断用户姓名、用户密码非空弄
        checkLoginParams(userName,userPassword);
        //2.根据用户名，查询用户信息
        User user = userMapper.queryUserByName(userName);
        //3.判断用户名是否为空
        AssertUtil.isTrue(null==user,"用户名不存在！");
        //4.判断密码是否正确
        checkUserPassword(userPassword,user.getUserPwd());
        return buildUserInfo(user);
    }

    /**
     * 修改用户密码
     1. 接收四个参数 （用户ID、原始密码、新密码、确认密码）
     2. 通过用户ID查询用户记录，返回用户对象
     3. 参数校验
     4. 设置用户的新密码
        需要将新密码通过指定算法进行加密（md5加密）
     5. 执行更新操作，判断受影响的行数

     * @param userId            用户id
     * @param oldPassword       老密码
     * @param newPassword       新密码
     * @param confirmPassword   确认密码
     */
    public void updateUserPassword(Integer userId, String oldPassword,
                        String newPassword, String confirmPassword ){
        //通过用户ID查询用户记录，返回用户对象
        User user = userMapper.selectByPrimaryKey(userId);
        //参数校验
        checkPasswordParams(user,oldPassword,newPassword,confirmPassword);
        //设置用户的新密码,需要将新密码通过指定算法进行加密（md5加密）
        user=new User();
        user.setId(userId);
        user.setUserPwd(Md5Util.encode(newPassword));
        AssertUtil.isTrue(userMapper.updateByPrimaryKeySelective(user)<1,"修改密码失败！");
    }

    /**
     待更新用户记录是否存在 （用户对象是否为空）
     判断原始密码是否为空
     判断原始密码是否正确（查询的用户对象中的用户密码是否原始密码一致）
     判断新密码是否为空
     判断新密码是否与原始密码一致 （不允许新密码与原始密码）
     判断确认密码是否为空
     判断确认密码是否与新密码一致
     * @param user              用户
     * @param oldPassword       老密码
     * @param newPassword       新密码
     * @param confirmPassword   确认密码
     */
    private void checkPasswordParams(User user, String oldPassword, String newPassword, String confirmPassword) {
        //待更新用户记录是否存在 （用户对象是否为空）
        AssertUtil.isTrue(null==user,"用户不存在！");
        //判断原始密码是否为空
        AssertUtil.isTrue(null==oldPassword ,"原始密码不能为空！");
        //判断原始密码是否正确（查询的用户对象中的用户密码是否原始密码一致）
        AssertUtil.isTrue(!(Md5Util.encode(oldPassword).equals(user.getUserPwd())),"原始密码错误！");
        //判断新密码是否为空
        AssertUtil.isTrue(null==newPassword,"新密码不能为空！");
        //判断新密码是否与原始密码一致 （不允许新密码与原始密码）
        AssertUtil.isTrue(oldPassword.equals(newPassword),"新密码和原始密码不能相同！");
        //判断确认密码是否为空
        AssertUtil.isTrue(null==confirmPassword,"确认密码不能为空！");
        //判断确认密码是否与新密码一致
        AssertUtil.isTrue(!(newPassword.equals(confirmPassword)),"确认密码和新密码不一致！");
    }

    /**
     *构造用户信息UserModel
     * @param user
     * @return
     */
    private UserModel buildUserInfo(User user) {
        UserModel userModel = new UserModel();
        //把数据放入模板
        userModel.setUserName(user.getUserName());
        //对id进行加密
        userModel.setUserIdStr(UserIDBase64.encoderUserID(user.getId()));
        userModel.setTrueName(user.getTrueName());
        return userModel;
    }

    /**
     * 判断密码是否正确，错误抛出异常，交给控制器处理
     * @param userPassword  用户输入id
     * @param userPwd   数据库查询密码
     */
    private void checkUserPassword(String userPassword, String userPwd) {
        //数据库的密码是MD5加密的，需要先把前台传来的密码进行加密在进行对比
        userPassword = Md5Util.encode(userPassword);
        //比较密码
        AssertUtil.isTrue(!userPwd.equals(userPassword),"密码错误!");
    }

    /**
     参数判断，判断用户姓名、用户密码非空弄
     如果参数为空，抛出异常（异常被控制层捕获并处理）
     * @param userName
     * @param userPassword
     */
    private void checkLoginParams(String userName, String userPassword) {
        //1.判断用户名
        AssertUtil.isTrue(StringUtils.isBlank(userName),"用户名不能为空！");
        //2.判断密码
        AssertUtil.isTrue(StringUtils.isBlank(userPassword),"密码不能为空！");
    }

    /**
     *
     */
    public List<Map<String,Object>> queryAllSales(){
        return userMapper.queryAllSales();
    }

    /**
     * 添加用户
     *  1. 参数校验
     *      用户名 非空 唯一性
     *      邮箱 非空
     *      手机号 非空 格式合法
     * 2. 设置默认参数
     *  isValid 1
     *      creteDate 当前时间
     *      updateDate 当前时间
     *      userPwd 123456 -> md5加密
     * 3. 执行添加，判断结果
     */
    @Transactional(propagation = Propagation.REQUIRED)
    public void saveUser(User user) {
        //1.参数校验
        checkAddParams(user.getId(),user.getUserName(),user.getEmail(),user.getPhone());
        //设置默认值
        user.setIsValid(1);
        user.setCreateDate(new Date());
        user.setUpdateDate(new Date());
        user.setUserPwd(Md5Util.encode("123456"));
        //执行添加操作
        AssertUtil.isTrue( userMapper.insertSelective(user) != 1,"用户添加失败");

        //添加用户角色关系

        relationUserRole(user.getId(),user.getRoleIds());
    }



    /**
     * 修改用户
     *  1. 参数校验
     *      用户名 非空 存在 唯一性
     *      邮箱 非空
     *      手机号 非空 格式合法
     * 2. 设置默认参数
     *      updateDate 当前时间
     * 3. 执行修改，判断结果
     */
    @Transactional(propagation = Propagation.REQUIRED)
    public void updateUser(User user) {
        //1.参数校验
        checkAddParams(user.getId(),user.getUserName(),user.getEmail(),user.getPhone());
        //设置默认值
        user.setUpdateDate(new Date());
        //执行添加操作
        AssertUtil.isTrue( userMapper.updateByPrimaryKeySelective(user) != 1,"用户修改失败");
        relationUserRole(user.getId(),user.getRoleIds());
    }

    /**
     * 添加用户和角色关系
     * 思路
     *      原来的有角色，就删除原来的角色关系，没有就直接添加角色关系
     * @param userId        用户id
     * @param roleIds   对应角色
     */
    @Transactional(propagation = Propagation.REQUIRED)
    public void relationUserRole(Integer userId, String roleIds) {
        //查存用户对应的角色数量
        int count=userRoleMapper.countUserRoleByUserId(userId);
        if (count>0){
            //删除现有的本用户的角色 ，通过用户的id删除所有的角色
            AssertUtil.isTrue(userRoleMapper.deleteUserRoleByUserId(userId) != count , "用户角色分配失败！");
        }
        if (StringUtils.isNotBlank(roleIds)){
            //重新添加用户角色
            ArrayList<UserRole> userRoles = new ArrayList<UserRole>();
            for (String s: roleIds.split(",")){
                UserRole userRole = new UserRole();
                userRole.setUserId(userId);
                userRole.setRoleId(Integer.parseInt(s));
                userRole.setCreateDate(new Date());
                userRole.setUpdateDate(new Date());
                userRoles.add(userRole);
            }
            AssertUtil.isTrue(userRoleMapper.insertBatch(userRoles) < userRoles.size() , "用户分配失败");
        }
    }

    /**
     * 数据校验
     */
    private void checkAddParams(Integer userId,String userName, String email, String phone) {
        //用户名不能为空，且不能已存在
        AssertUtil.isTrue(StringUtils.isBlank(userName),"用户名不能为空！");
        User temp = userMapper.queryUserByName(userName);
        AssertUtil.isTrue(null != temp && !(temp.getId().equals(userId)), "该用户已存在！");
        //email要符合格式
        AssertUtil.isTrue(StringUtils.isBlank(email),"邮箱不能为空！");
        AssertUtil.isTrue(!EmailUtil.isEmail(email),"邮箱格式不正确！");
        //手机号的验证
        AssertUtil.isTrue(StringUtils.isBlank(phone),"手机好不能为空！");
        AssertUtil.isTrue(!PhoneUtil.isMobile(phone),"手机号格式不正确！");
    }


    /**
     * 单个批量删除用户
     * @param ids   用户id数组
     */
    @Transactional(propagation = Propagation.REQUIRED)
    public void deleteBatchById(Integer[] ids) {
        AssertUtil.isTrue(null ==ids || ids.length < 1,"待删除记录不存在！");
        AssertUtil.isTrue(userMapper.deleteBatch(ids) <0 ,"删除失败！");
        //循环删除每个用户的角色关系
        for (Integer id : ids){
            //判断角色是否绑定角色
            int count = userRoleMapper.countUserRoleByUserId(id);
            if (count > 0) {
                //存在角色，删除角色关系
                AssertUtil.isTrue(userRoleMapper.deleteUserRoleByUserId(id) != count, "用户角色删除失败！");
            }
        }
    }

    public List<Map<String, Object>> queryAllCustomerManagers() {
        return userMapper.queryAllCustomerManagers();
    }
}
