package com.wzz.crm.service;

import com.wzz.crm.base.BaseService;
import com.wzz.crm.dao.OrderDetailsMapper;
import com.wzz.crm.domain.OrderDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * 订单详情服务类
 */
@Service
public class OrderDetailsService extends BaseService<OrderDetails,Integer> {

    @Autowired
    private OrderDetailsMapper orderDetailsMapper;

}
