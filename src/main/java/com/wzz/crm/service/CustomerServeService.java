package com.wzz.crm.service;

import com.wzz.crm.base.BaseService;
import com.wzz.crm.dao.CustomerMapper;
import com.wzz.crm.dao.CustomerServeMapper;
import com.wzz.crm.dao.UserMapper;
import com.wzz.crm.domain.CustomerServe;
import com.wzz.crm.enums.CustomerServeStatus;
import com.wzz.crm.utils.AssertUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;

/**
 * 客户服务的服务类
 */
@Service
public class CustomerServeService extends BaseService<CustomerServe,Integer> {

    @Autowired
    private CustomerServeMapper customerServeMapper;

    @Autowired
    private CustomerMapper customerMapper;

    @Autowired
    private UserMapper userMapper;

    /**
     * 添加客户服务
     *  1.参数校验
     *      客户名 非空 存在
     *      服务类型 非空
     *      服务内容 非空
     *      创建人     当前登录用户（去前端在cookie获取）
     *  2.设置默认值
     *      服务状态 创建状态 fw_001
     *      有效值 1
     *      创建时阿
     *      更新时间
     *  3.执行添加操作，判断返回值
     * @param customerServe     待添加的服务信息
     */
    @Transactional(propagation = Propagation.REQUIRED)
    public void addCustomerServe(CustomerServe customerServe) {
        //1.参数校验
        //客户名 非空 存在
        AssertUtil.isTrue(StringUtils.isNotBlank(customerServe.getCustomer()), "客户名不能为空！");
        AssertUtil.isTrue(null == customerMapper.queryCustomerByName(customerServe.getCustomer()), "客户名不存在！");
        //服务类型 非空
        AssertUtil.isTrue(StringUtils.isBlank(customerServe.getServeType()) , "请选择服务类型！");
        //服务内容 非空
        AssertUtil.isTrue(StringUtils.isNotBlank(customerServe.getServiceRequest()),"服务内容不能为空！");
        //创建人     当前登录用户（去前端在cookie获取）
        AssertUtil.isTrue(null==customerServe.getCreatePeople(),"创建人参数异常！");
        //2.设置默认值
        //服务状态 创建状态 fw_001
        customerServe.setState(CustomerServeStatus.CREATED.getState());
        //有效值 1
        customerServe.setIsValid(1);
        //创建时间
        customerServe.setCreateDate(new Date());
        //更新时间
        customerServe.setUpdateDate(new Date());
        //3.执行添加操作，判断返回值
        AssertUtil.isTrue(customerServeMapper.insertSelective(customerServe) != 1, "添加失败！");

    }

    /**
     * 客户服务的更新
     * 服务分配/服务处理/服务反馈
     *  1.校验数据
     *      客户服务id  非空 存在
     *      客户服务状态
     *          如果服务状态 为分配状态 fw_002
     *              分配人 非空  分配用户记录存在
     *              分配时间
     *          如果服务状态为 处理状态 fw_003
     *              处理内容 非空
     *              处理时间
     *          如果状态是 服务反馈状态 fw_004
     *              反馈内容 非空
     *              反馈满意度 非空
     *              服务状态设置为 fw_005
     *     修改时间
     *  2.执行更新操作，判断返回结果
     *
     */
    @Transactional(propagation = Propagation.REQUIRED)
    public void updateCustomerServe(CustomerServe customerServe) {
        //客户服务id 非空 存在
        AssertUtil.isTrue(null == customerServe.getId() ||
                customerServeMapper.selectByPrimaryKey(customerServe.getId()) == null, "待更新记录不存在！");
        //判断客户服务状态
        if (CustomerServeStatus.ASSIGNED.getState().equals(customerServe.getState())){
            //服务分配
            //分配人  非空 分配用户存在
            AssertUtil.isTrue(StringUtils.isBlank(customerServe.getAssigner()), "待分配用户不能为空！");
            AssertUtil.isTrue(null==userMapper.selectByPrimaryKey(Integer.parseInt(customerServe.getAssigner())),"分配用户不存在！");
            customerServe.setAssignTime(new Date());

        }else if (CustomerServeStatus.PROCED.getState().equals(customerServe.getState())){
            //服务处理操作
            //处理内容 非空
            AssertUtil.isTrue(StringUtils.isBlank(customerServe.getServiceProce()),"服务处理内容不能为空！");
            customerServe.setServiceProceTime(new Date());
        }else if (CustomerServeStatus.FEED_BACK.getState().equals(customerServe.getState())){
            //服务反馈操作
            //反馈内容非空
            AssertUtil.isTrue(StringUtils.isBlank(customerServe.getServiceProceResult()), "服务反馈内容不能为空！");
            //服务满意度非空
            AssertUtil.isTrue(StringUtils.isBlank(customerServe.getMyd()), "请选择服务反馈啊满意度！");
            //服务状态设置为归档
            customerServe.setState(CustomerServeStatus.ARCHIVED.getState());
        }
        //设置更新时间
        customerServe.setUpdateDate(new Date());
        //执行更新操作
        AssertUtil.isTrue(customerServeMapper.updateByPrimaryKeySelective(customerServe) != 1, "操作失败！");
    }
}
