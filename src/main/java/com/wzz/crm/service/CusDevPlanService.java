package com.wzz.crm.service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.wzz.crm.base.BaseService;
import com.wzz.crm.dao.CusDevPlanMapper;
import com.wzz.crm.dao.SaleChanceMapper;
import com.wzz.crm.domain.CusDevPlan;
import com.wzz.crm.query.CusDevPlanQuery;
import com.wzz.crm.utils.AssertUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 客人开发计划服务
 */
@Service
public class CusDevPlanService extends BaseService<CusDevPlan,Integer> {

    @Autowired
    private CusDevPlanMapper cusDevPlanMapper;

    @Autowired
    private SaleChanceMapper saleChanceMapper;
    /**
     * 多条件查询计划项列表
     * @param query 查询条件
     */
    public Map<String ,Object> queryCusDevPlansByParams(CusDevPlanQuery query){
        HashMap<String, Object> map = new HashMap<>();
        //开启分页助手
        PageHelper.startPage(query.getPage(),query.getLimit());
        PageInfo<CusDevPlan> pageInfo = new PageInfo<>(cusDevPlanMapper.selectByParams(query));
        //对数据进行封装，格式固定
        /*code:0  //数据状态
        msg:""  //状态信息
        count:1000  //数据总数
        data:[]  //数据列表*/
        map.put("code",0);
        map.put("msg","success");
        map.put("count",pageInfo.getTotal());
        map.put("data",pageInfo.getList());
        return map;
    }

    /**
     * 添加计划项
     *  1.参数校验
     *      营销机会id 不为空且查询数据存在
     *      计划项内容 非空
     *      计划项时间 非空
     *  2.设置默认参数
     *      isValid 1
     *      createDate  当前时间
     *      updateDate 当前时间
     * 3.执行操作，判断结果
     * @param cusDevPlan    计划项对象
     */
    @Transactional(propagation = Propagation.REQUIRED)
    public void saveCusDevPlan(CusDevPlan cusDevPlan){
        //参数校验
        checkParams(cusDevPlan.getSaleChanceId(),cusDevPlan.getPlanItem(),cusDevPlan.getPlanDate());
        //设置相关参数
        cusDevPlan.setCreateDate(new Date());
        cusDevPlan.setIsValid(1);
        cusDevPlan.setUpdateDate(new Date());
        //执行操作，判断结果
        AssertUtil.isTrue(cusDevPlanMapper.insertSelective(cusDevPlan) != 1 ,"计划添加失败！");

    }

    /**
     * 更新开发计划
     *  1.校验数据
     *      比添加的多校验一条开发计划的id 不能为空
     *  2.设置默认值
     *      更行时间为当前时间
     * 3.执行操作
     *      判断返回值
     * @param cusDevPlan    更新的时间（必含id）
     */
    public void updateCusDevPlan(CusDevPlan cusDevPlan){
        //校验数据
        AssertUtil.isTrue(null == cusDevPlan.getId(),"开发计划不存在！");
        checkParams(cusDevPlan.getSaleChanceId(),cusDevPlan.getPlanItem(),cusDevPlan.getPlanDate());
        //设置默认值  更新时间
        cusDevPlan.setUpdateDate(new Date());
        //执行操作
        AssertUtil.isTrue(cusDevPlanMapper.updateByPrimaryKeySelective(cusDevPlan) != 1,"更新失败！");
    }

    /**
     * 校验参数，错误抛出异常
     * 营销机会id 不为空且查询数据存在
     * 计划项内容 非空
     * 计划项时间 非空
     * @param saleChanceId  营销机会id
     * @param planItem      计划项内容
     * @param planDate      计划项时间
     */
    private void checkParams(Integer saleChanceId, String planItem, Date planDate) {
        AssertUtil.isTrue(null == saleChanceId,"营销机会ID不存在");
        AssertUtil.isTrue(null == saleChanceMapper.selectByPrimaryKey(saleChanceId),"营销机会不存在！");
        AssertUtil.isTrue(StringUtils.isBlank(planItem),"请输入计划内容！");
        AssertUtil.isTrue(null == planDate,"请输入计划时间！");
    }


    /**
     * 开发计划的删除，实际是更改有效值为0
     *  1.校验数据
     *      id 非空 记录存在
     *  2.修改有效值
     *      改为为0
     *  3.执行更行操作，判断返回结果
     * @param id    要删除的开发计划id
     */
    @Transactional(propagation = Propagation.REQUIRED)
    public void deleteCusDevPlan(Integer id) {
        //1.校验数据 id 非空 记录存在
        AssertUtil.isTrue(null == id ,"待删除数据不存在！");
        // 判断记录是否记录存在
        CusDevPlan cusDevPlan = cusDevPlanMapper.selectByPrimaryKey(id);
        AssertUtil.isTrue(null == cusDevPlan ,"待删除数据不存在!");
        // 2.修改有效值
        cusDevPlan.setIsValid(0);
        //执行更新操作，判断返回结果
        AssertUtil.isTrue(cusDevPlanMapper.updateByPrimaryKeySelective(cusDevPlan) != 1,"开发计划删除失败！");
    }
}
