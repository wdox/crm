package com.wzz.crm.service;

import com.wzz.crm.base.BaseService;
import com.wzz.crm.dao.PermissionMapper;
import com.wzz.crm.domain.Permission;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 权限的服务类
 */
@Service
public class PermissionService extends BaseService<Permission,Integer> {

    @Autowired
    private PermissionMapper permissionMapper;

    //通过用户id查询用户所有的权限吗
    public List<String> queryAllAclValueByUserId(Integer userId) {
        return permissionMapper.queryAllAclValueByUserId(userId);
    }
}
