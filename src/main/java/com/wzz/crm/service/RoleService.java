package com.wzz.crm.service;


import com.wzz.crm.base.BaseService;
import com.wzz.crm.dao.ModuleMapper;
import com.wzz.crm.dao.PermissionMapper;
import com.wzz.crm.dao.RoleMapper;
import com.wzz.crm.domain.Permission;
import com.wzz.crm.domain.Role;
import com.wzz.crm.utils.AssertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 角色有关的服务类
 */
@Service
public class RoleService extends BaseService<Role,Integer> {

    @Autowired
    private RoleMapper roleMapper;

    @Autowired
    private PermissionMapper permissionMapper;

    @Autowired
    private ModuleMapper moduleMapper;

    /**
     * 查询所有角色，id不为空时，查询对应用户的说有角色
     * @param userId    用户id
     */
    public List<Map<String, Object>> queryAllRoles(Integer userId) {
        return roleMapper.queryAllRoles(userId);
    }

    /**
     * 角色的添加
     *  1.数据校验
     *      角色名称 ：不能为空，且不能重复
     *  2.设置默认值
     *      是否有效    1
     *      创建时间    当前时间
     *      修改时间    当前时间
     * 3.执行添加操作，判断结果
     * @param role  要添加的角色信息
     */
    public void addRole(Role role) {
        AssertUtil.isTrue(null == role ,"待添加数据存在！");
        //1.数据校验
        AssertUtil.isTrue(null == role.getRoleName(),"角色名称不能为空！");
        AssertUtil.isTrue( null != roleMapper.selectByRoleName(role.getRoleName()) ,"角色名称已经存在！");
        //2.设置默认值
        role.setIsValid(1);
        role.setCreateDate(new Date());
        role.setUpdateDate(new Date());
        //3.执行添加操作
        AssertUtil.isTrue(roleMapper.insertSelective(role) != 1 ,"添加角色失败！");
    }

    /**
     * 角色信息更新
     *  1.数据校验
     *      角色名： 不为空 切不存在
     *              通过角色名查询角色信息
     *                  存在判断id是否相同，相同则通过
     *                  不相同抛出异常
     * 2.设置默认值
     *      修改时间 当前时间
     * 3.执行更行操作 ，判断结果
     * @param role  待更新的角色信息
     */
    @Transactional(propagation = Propagation.REQUIRED)
    public void updateRole(Role role) {
        AssertUtil.isTrue( null == role ,"待更新的角色不存在！");
        AssertUtil.isTrue(null == role.getRoleName() , "角色名不能为空！");
        //通过角色名查询角色
        Role tempRole = roleMapper.selectByRoleName(role.getRoleName());
        AssertUtil.isTrue(null !=tempRole && tempRole.getId() != role.getId(),"角色名已经存在！");
        //2.设置默认值
        role.setUpdateDate(new Date());
        //3.执行更新操作
        AssertUtil.isTrue(roleMapper.updateByPrimaryKeySelective(role) != 1 ,"角色信息更新失败！");
    }

    /**
     * 逻辑删除角色的id（实际是更新）
     *  1.判断id是否存在
     *  2.设置默认值
     *      有效值  0
     *      更新时间   当前时间
     * 3.执行更新操作
     * @param roleId    代删除的角色id
     */
    @Transactional(propagation = Propagation.REQUIRED)
    public void deleteByRoleId(Integer roleId) {
        AssertUtil.isTrue(null == roleId , "待删除id不能为空！");
        //通过id查询角色
        Role role = roleMapper.selectByPrimaryKey(roleId);
        AssertUtil.isTrue(null == role , "待更新记录不存在！");
        //设置默认值
        role.setIsValid(0);
        role.setUpdateDate(new Date());
        AssertUtil.isTrue(roleMapper.updateByPrimaryKeySelective(role) != 1 ,"角色删除失败！");
    }

    /**
     * 角色的授权操作
     *  1.通过角色id查询本角色是否已经有权限的绑定
     *  2.有绑定则删除原来的
     *  3.判断mIds是否为空
     *      为空不添加了
     *      不为空，创建权限对象集合，
     *          每个对象的默认值
     *              roleId      都是这一个roleId
     *              moduleId    mIds挨个写入
     *              权限码       去查询没得mId对应的权限码
     *              更新和创建时间 当前时间
     *  4.执行添加批量添加操作
     * @param mIds      模板的id集合
     * @param roleId    角色的id
     */
    @Transactional(propagation = Propagation.REQUIRED)
    public void addGrant(Integer[] mIds, Integer roleId) {
        int count=permissionMapper.countPermissionByRoleId(roleId);
        //判断 统计的数量
        if (count > 0){
            //存在，则删除原来的权限
            AssertUtil.isTrue(permissionMapper.deletePermissionByRoleId(roleId) != count,"授权失败！");
        }
        if (null != mIds && mIds.length > 0){
            //权限集合存在，创建权限的对象集合
            ArrayList<Permission> permissions = new ArrayList<>();
            for (Integer mid : mIds){
                //遍历mIds
                Permission permission = new Permission();
                permission.setRoleId(roleId);
                permission.setModuleId(mid);
                permission.setAclValue(moduleMapper.selectByPrimaryKey(mid).getOptValue());
                permission.setCreateDate(new Date());
                permission.setUpdateDate(new Date());
                permissions.add(permission);
            }
            //执行更新操作，判断结果
            AssertUtil.isTrue(permissionMapper.insertBatch(permissions) != permissions.size() , "授权失败！");

        }
    }
}
