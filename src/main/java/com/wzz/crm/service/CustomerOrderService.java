package com.wzz.crm.service;

import com.wzz.crm.base.BaseService;
import com.wzz.crm.dao.CustomerOrderMapper;
import com.wzz.crm.domain.CustomerOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * 客户订单服务
 */
@Service
public class CustomerOrderService extends BaseService<CustomerOrder,Integer> {

    @Autowired
    private CustomerOrderMapper customerOrderMapper;

    /**
     * 通过id查询订单信息
     * @param orderId   订单id
     * @return          （id,order_no,address,status，total）
     */
    public Map<String, Object> queryOrderAndTotalById(Integer orderId) {
        return customerOrderMapper.queryOrderAndTotalById(orderId);
    }
}
