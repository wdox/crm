package com.wzz.crm.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.wzz.crm.base.BaseService;
import com.wzz.crm.dao.CustomerLossMapper;
import com.wzz.crm.dao.CustomerMapper;
import com.wzz.crm.dao.CustomerOrderMapper;
import com.wzz.crm.domain.Customer;
import com.wzz.crm.domain.CustomerLoss;
import com.wzz.crm.domain.CustomerOrder;
import com.wzz.crm.query.CustomerQuery;
import com.wzz.crm.utils.AssertUtil;
import com.wzz.crm.utils.PhoneUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * 客户服务层
 */
@Service
public class CustomerService extends BaseService<Customer,Integer> {

    @Autowired
    private CustomerMapper customerMapper;

    @Autowired
    private CustomerLossMapper customerLossMapper;

    @Autowired
    private CustomerOrderMapper customerOrderMapper;

    /**
     * 客户的添加
     *  1.参数校验
     *      客户名称 name 不为空 不可重复
     *      联系电话 phone 非空 符合手机号格式
     *      法人 非空
     *  2.设置默认值
     *      isValid 1
     *      state 0
     *      创建时间 和 更新时间 当前时间
     *      khno 客户编号 唯一  方案（uuid | 时间戳 | 年月日时分秒 雪花算法）
     *  3.执行添加操作，判断受影响行数
     * @param customer  带甜的客户的信息
     */
    @Transactional(propagation = Propagation.REQUIRED)
    public void saveCustomer(Customer customer){
        //1.校验参数
        checkParams(customer.getName(), customer.getPhone(), customer.getFr());
        //判断客户名称是否存在，通过name查询客户
        AssertUtil.isTrue(null != customerMapper.queryCustomerByName(customer.getName()), "客户名称已经存在！");
        //2.设置默认值
        customer.setIsValid(1);
        customer.setState(0);
        customer.setKhno("KH" + System.currentTimeMillis());
        customer.setCreateDate(new Date());
        customer.setUpdateDate(new Date());
        //执行添加操作，判断返回结果
        AssertUtil.isTrue(customerMapper.insertSelective(customer) != 1, "添加失败！");
    }

    /**
     * 客户的修改
     *  1.参数校验
     *      id 不能为空 且存在
     *      客户名称 name 不为空 不可重复
     *      联系电话 phone 非空 符合手机号格式
     *      法人 非空
     *  2.设置默认值
     *      更新时间 当前时间
     *  3.执行更新操作，判断受影响行数
     * @param customer  待更新的客户的信息
     */
    @Transactional(propagation = Propagation.REQUIRED)
    public void updateCustomer(Customer customer){
        //1.校验参数
        //判断id
        AssertUtil.isTrue(null == customer.getId(), "待更新记录不存在（原因：没有ID）");
        //通过id查询
        AssertUtil.isTrue(null==customerMapper.selectByPrimaryKey(customer.getId()),"待更新记录不存在（无记录）!");
        checkParams(customer.getName(), customer.getPhone(), customer.getFr());
        //判断客户名称是否存在，通过name查询客户
        Customer temp = customerMapper.queryCustomerByName(customer.getName());
        //判断是否为空
        if (null != temp){
            AssertUtil.isTrue(!customer.getId().equals(temp.getId()) , "客户名称已经存在！");
        }
        //2.设置默认值
        customer.setUpdateDate(new Date());
        //执行添加操作，判断返回结果
        AssertUtil.isTrue(customerMapper.updateByPrimaryKeySelective(customer) != 1, "修改失败！");
    }


    /**
     * 判断参数，都不能为空
     * @param name      客户名称
     * @param phone     手机号
     * @param fr        法人
     */
    private void checkParams(String name, String phone, String fr) {
        AssertUtil.isTrue(StringUtils.isBlank(name), "客户名称不能为空！");
        AssertUtil.isTrue(StringUtils.isBlank(fr), "法人不能为空！");
        AssertUtil.isTrue(StringUtils.isBlank(phone), "手机号不能为空！");
        AssertUtil.isTrue(!PhoneUtil.isMobile(phone),"手机号格式不正确！");
    }

    /**
     * 删除客户信息 (逻辑删除 ，修改有效值)
     *  1.校验数据
     *      id 不为空且存在
     *  2.设置默认值
     *      isValid 0
     *      更新时间 当前时间
     *  3.执行更新操作，判断返回结果
     * @param id
     */
    @Transactional(propagation = Propagation.REQUIRED)
    public void deleteCustomer(Integer id) {
        //校验数据
        AssertUtil.isTrue(null == id, "待删除客户不存在！（1）");
        Customer customer = customerMapper.selectByPrimaryKey(id);
        AssertUtil.isTrue(null == customer , "待删除客户不存在！");
        //2.设置默认值
        customer.setIsValid(0);
        customer.setUpdateDate(new Date());
        //执行更新操作，判断返回结果
        AssertUtil.isTrue(customerMapper.updateByPrimaryKeySelective(customer) != 1, "删除失败！");
    }

    /**
     * 更新客户流失状态
     *  1.查询待流失的客户数据
     *  2.将流失库客户批量添加到客户流失表中
     *  3.批量更新客户流失状态 state = 1 （表示流失状态）
     */
    @Transactional(propagation = Propagation.REQUIRED)
    public void updateCustomerLossState(){
        //1.查询待流失的客户数据
        List<Customer> lossCustomerList = customerMapper.queryLossCustomers();
        //2.将流失库客户批量添加到客户流失表中
        if (null != lossCustomerList && lossCustomerList.size() > 0) {
            //流失客户存在
            //创建流失客户集合，用于下面进行批量添加流失客户向流失客户表中
            List<CustomerLoss> customerLossList =new ArrayList<>();
            //创建一个存放流失客户id的集合，用于下面进行批量的流失状态更新
            List<Integer> ids = new ArrayList<>();
            //循环遍历，把流失客户放入流失客户集合
            lossCustomerList.forEach(customer -> {
                CustomerLoss customerLoss = new CustomerLoss();
                //设置客户编号
                customerLoss.setCusNo(customer.getKhno());
                //设置客户经理
                customerLoss.setCusManager(customer.getCusManager());
                //设置客户名称
                customerLoss.setCusName(customer.getName());
                //流失客户创建时间
                customerLoss.setCreateDate(new Date());
                //设置更新时间
                customerLoss.setUpdateDate(new Date());
                //设置有效状态
                customerLoss.setIsValid(1);
                //客户暂缓状态 0 展缓流失状态 1确认流失状态
                customerLoss.setState(0);
                //设置最后下单时间
                //通过客户id查询客户最后一次的下单记录
                CustomerOrder lastCustomerOrder=customerOrderMapper.queryLastOrderByCustomerId(customer.getId());
                if (null != lastCustomerOrder) {
                    customerLoss.setLastOrderTime(lastCustomerOrder.getOrderDate());
                }
                //把流失客户放入集合
                customerLossList.add(customerLoss);
                //把当前流失客户的id放入流失客户id集合
                ids.add(customer.getId());
            });
            AssertUtil.isTrue(customerLossMapper.insertBatch(customerLossList)!=customerLossList.size(),"流失客户更新失败！（1）");
            //客户表的流失状态的更新
            AssertUtil.isTrue(customerMapper.updateStateBatchByIds(ids) != ids.size(), "流失客户更新失败（2）！");
        }
    }


    /**
     * 多条件分页查询查询贡献值 客户列表
     * @param customerQuery     查询条件
     */
    public Map<String, Object> queryCustomerContributionByParams(CustomerQuery customerQuery) {
        Map<String,Object> result = new HashMap<String,Object>();
        PageHelper.startPage(customerQuery.getPage(),customerQuery.getLimit());
        PageInfo<Map<String,Object>> pageInfo =new PageInfo<Map<String,Object>>(customerMapper.queryCustomerContributionByParams(customerQuery));
        result.put("count",pageInfo.getTotal());
        result.put("data",pageInfo.getList());
        result.put("code",0);
        result.put("msg","");
        return result;
    }

    /**
     * 图形表报的数据处理（柱状图）
     *     data1: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
     *     data2: [150, 230, 224, 218, 135, 147, 260]
     */
    public Map<String, Object> countCustomerMake() {

        Map<String, Object> map = new HashMap<>();
        //查询数据库获得x轴标题（客户类型名）和和数据（每个类型的数量）
        List<Map<String,Object>> dataList=customerMapper.countCustomerMake();
        //存放x的标题
        ArrayList<String> data1 = new ArrayList<>();
        //存放X轴的数据
        ArrayList<Object> data2 = new ArrayList<>();
        if (null != dataList && dataList.size() > 0) {
            dataList.forEach(m->{
                data1.add(m.get("level").toString());
                /*data2.add(Integer.parseInt(m.get("num").toString()) );*/
                data2.add(m.get("num"));
            });
        }
        map.put("data1", data1);
        map.put("data2", data2);
        return map;
    }

    /**
     * 图形表报的数据处理（饼状图）
     * data1: ['rose1','rose2','rose3','rose4']
     * data2: [{ value: 40, name: 'rose 1' },{ value: 33, name: 'rose 2' },
     *         { value: 28, name: 'rose 3' },{ value: 22, name: 'rose 4' }]
     */
    public Map<String, Object> countCustomerMake02() {
        Map<String, Object> map = new HashMap<>();
        //查询数据库获得x轴标题（客户类型名）和和数据（每个类型的数量）
        List<Map<String,Object>> dataList=customerMapper.countCustomerMake();
        //存放x的标题
        ArrayList<String> data1 = new ArrayList<>();
        //存放X轴的数据
        ArrayList<Map<String,Object>> data2 = new ArrayList<>();
        if (null != dataList && dataList.size() > 0) {
            dataList.forEach(m->{
                data1.add(m.get("level").toString());
                HashMap<String, Object> levelAndNum = new HashMap<>();
                levelAndNum.put("value",m.get("num"));
                levelAndNum.put("name", m.get("level"));
                data2.add(levelAndNum);
            });
        }
        map.put("data1", data1);
        map.put("data2", data2);
        return map;
    }
}
