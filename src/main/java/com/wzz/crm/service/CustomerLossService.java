package com.wzz.crm.service;

import com.wzz.crm.base.BaseService;
import com.wzz.crm.dao.CustomerLossMapper;
import com.wzz.crm.domain.CustomerLoss;
import com.wzz.crm.utils.AssertUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * 客户流失服务类
 */
@Service
public class CustomerLossService extends BaseService<CustomerLoss,Integer> {
    @Autowired
    private CustomerLossMapper customerLossMapper;

    /**
     * 确认流失
     *  1.参数校验
     *      id 不为空 存在
     *      原因 不能为空
     *  2.设置默认值
     *      状态state 1
     *      修改时间
     *      确认流失时间
     *  3.执行更新操作，判断返回结果
     * @param id            确认流失的客户的id
     * @param lossReason    流失原因
     */
    public void updateCustomerLossStateById(Integer id, String lossReason) {
        //1.参数校验
        //id 不为空 存在
        AssertUtil.isTrue(null == id, "id不能为空！");
        AssertUtil.isTrue(null == customerLossMapper.selectByPrimaryKey(id), "流失客户不存在！");
        //原因 不能为空
        AssertUtil.isTrue(StringUtils.isBlank(lossReason), "流失原因不能为空！");
        //设置默认值
        CustomerLoss customerLoss = new CustomerLoss();
        customerLoss.setId(id);
        customerLoss.setState(1);
        customerLoss.setLossReason(lossReason);
        customerLoss.setUpdateDate(new Date());
        customerLoss.setConfirmLossTime(new Date());
        //3.执行更新操作，判断返回结果
        AssertUtil.isTrue(customerLossMapper.updateByPrimaryKeySelective(customerLoss) != 1, "确认流失失败！");
    }
}
