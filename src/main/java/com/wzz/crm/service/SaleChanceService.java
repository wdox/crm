package com.wzz.crm.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.wzz.crm.base.BaseService;
import com.wzz.crm.dao.SaleChanceMapper;
import com.wzz.crm.domain.SaleChance;
import com.wzz.crm.enums.DevResult;
import com.wzz.crm.enums.StateStatus;
import com.wzz.crm.query.SaleChanceQuery;
import com.wzz.crm.utils.AssertUtil;
import com.wzz.crm.utils.PhoneUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


import java.util.Date;
import java.util.HashMap;

/**
 * wzz 创建于 2022/4/14 14:52
 * 营销机会服务
 **/
@Service
public class SaleChanceService extends BaseService<SaleChance,Integer> {

    @Autowired
    private SaleChanceMapper saleChanceMapper;

    /**
     * 分页分类查询
     * @param query 营销关系分页多条件查询模板类
     * @return  map集合（msg，code，list<SaleChance>）
     */
    public HashMap<String, Object> querySaleChanceByParams(SaleChanceQuery query){
        HashMap<String, Object> map = new HashMap<>();
        //开启分页，使用分页助手
        PageHelper.startPage(query.getPage(),query.getLimit());
        PageInfo<SaleChance> info=new PageInfo<>(saleChanceMapper.selectByParams(query));
        //对数据进行封装，格式固定
        /*code:0  //数据状态
        msg:""  //状态信息
        count:1000  //数据总数
        data:[]  //数据列表*/
        map.put("code",0);
        map.put("msg","success");
        map.put("count",info.getTotal());
        map.put("data",info.getList());
        return map;
    }

    /**
     * 添加一条新的营销关系
     * 营销机会数据添加服务
     *  1.参数校验
     *      customerName：非空
     *      linkMan：非空
     *      LinkPhone:非空 手机号格式
     * 2.设置想换参数默认值
     *      state： 默认未分配 ，如果有分配人，则已分配
     *      assignTime :有分配人 ，就是当前系统时间
     *      devResult ：，默认未开发，有分配人为开发中 ，0-未开发 1-开发中 2-开发成功 3-开发失败
     *      isValid：默认有效数据（1-有效 ，0-无效）
     *      createTime updateTime: d当前系统时间
     * 3.执行添加，判断并返回结果
     * @param saleChance    营销关系对象
     */
    //添加操作开启事务
    @Transactional(propagation = Propagation.REQUIRED)
    public void addSaleChance(SaleChance saleChance){
        //判断参数
        checkParams(saleChance.getCustomerName(),saleChance.getLinkMan(),saleChance.getLinkPhone());
        //分配人是否为空或长度为0或由空白符
        if (StringUtils.isBlank(saleChance.getAssignMan())){
            //没有分配人，设置分配状态和开发结果
            saleChance.setState(StateStatus.UNSTATE.getType());
            saleChance.setDevResult(DevResult.UNDEV.getStatus());
        }else{
            //有分配人 ，设置分配时间，分配状态和开发状态
            saleChance.setState(StateStatus.STATED.getType());  //开发状态
            saleChance.setDevResult(DevResult.DEVING.getStatus());  //开发中
            saleChance.setAssignTime(new Date());   //分配时间
        }
        //设置有效值
        saleChance.setIsValid(1);
        //创建时间和修改时间
        saleChance.setCreateDate(new Date());
        saleChance.setUpdateDate(new Date());

        //执行添加操作，判断结果
        AssertUtil.isTrue(saleChanceMapper.insertSelective(saleChance) < 1,"营销机会添加失败！");

    }

    /**
     * 营销机会更新
     * 1.参数检查
     *      id customerName linkMan LinkPhone：不能为空
     *      并且 LinkPhone 为手机格式
     * 2.设置相关属性
     *      原始未分配
     *         更新未分配 ： 不操作
     *         更新有分配 ：
     *              状态 state（0-->1）
     *              分配时间 assignTime（当前系统时间）
     *              开发状态 devResult (0-->1)
     *     原始已分配
     *          更新未分配
     *              状态 state（1-->0）
     *              分配时间 assignTime（null）
     *              开发状态 devResult (1-->0)
     *         更新有分配
     *              与当前分配人相同： 不做操作
     *              与当前不一致
     *                  修改分配人
     * 3.执行操作，判断结果
     * @param saleChance    营销机会更新的数据
     */
    @Transactional(propagation = Propagation.REQUIRED)
    public void updateSaleChance(SaleChance saleChance){
        //参数检验
        //判断id是否为空
        AssertUtil.isTrue(null == saleChance.getId(),"待更新记录不存在！");
        //使用id查询营销机会
        SaleChance temp = saleChanceMapper.selectByPrimaryKey(saleChance.getId());
        AssertUtil.isTrue(null == temp ,"待更新记录不存在！");
        //校验基础参数
        checkParams(saleChance.getCustomerName(),saleChance.getLinkMan(),saleChance.getLinkPhone());
        //设置相关参数
        //原始未分配
        if (StringUtils.isBlank(temp.getAssignMan())){
            // 更新未分配 ： 不操作
            if (StringUtils.isNotBlank(saleChance.getAssignMan())){
                //更新有分配 ：
                //状态 state（0-->1）
                saleChance.setState(StateStatus.STATED.getType());
                //分配时间 assignTime（当前系统时间）
                saleChance.setAssignTime(new Date());
                //开发状态 devResult (0-->1)
                saleChance.setDevResult(DevResult.DEVING.getStatus());
            }
        }else{ //原始已分配
            if (StringUtils.isBlank(saleChance.getAssignMan())){
                //更新未分配
                //状态 state（1-->0）
                saleChance.setState(StateStatus.UNSTATE.getType());
                //分配时间 assignTime（null）
                saleChance.setAssignTime(null);
                //开发状态 devResult (1-->0)
                saleChance.setDevResult(DevResult.UNDEV.getStatus());
            }else{
                //更新有分配



                if (!temp.getAssignMan().equals(saleChance.getAssignMan())) {
                    //与当前不一致
                    //修改分配人
                    saleChance.setAssignTime(new Date());
                }else{
                    //与当前分配人相同： 继承原来的分配时间
                    saleChance.setAssignTime(temp.getAssignTime());
                }


            }
        }
        //设置修改时间
        saleChance.setUpdateDate(new Date());
        //执行方法判断结果
        AssertUtil.isTrue(saleChanceMapper.updateByPrimaryKeySelective(saleChance) !=1 ,"营销机会更新失败！");
    }

    /**
     * 参数检查
     * @param customerName  机会名
     * @param linkMan       联系人
     * @param linkPhone     联系电话
     * 如果格式不对，抛出异常
     */
    private void checkParams(String customerName, String linkMan, String linkPhone) {
        //customerName：非空
        AssertUtil.isTrue(null==customerName,"营销机会名不能为空！");
        //linkMan：非空
        AssertUtil.isTrue(null==linkMan,"联系人不能为空！");
        //LinkPhone:非空 手机号格式
        AssertUtil.isTrue(null==linkPhone,"营销机会名不能为空！");
        AssertUtil.isTrue(!PhoneUtil.isMobile(linkPhone),"联系人电话格式不正确！");
    }

    /**
     * 批量删除营销关系
     * @param ids   id的数组
     */
    @Transactional(propagation = Propagation.REQUIRED)
    public void deleteSaleChance(Integer[] ids){
        //判断ids是否为空
        AssertUtil.isTrue((null == ids || ids.length <1),"待删除信息不存在！");
        AssertUtil.isTrue(saleChanceMapper.deleteBatch(ids) != ids.length ,"删除失败！");
    }

    /**
     * 更新营销机会开发状态
     *  2开发成功
     *  3开发失败
     * @param sid           id
     * @param devResult     开发状态
     */
    @Transactional(propagation = Propagation.REQUIRED)
    public void updateSaleChanceDevResult(Integer sid, Integer devResult) {
        AssertUtil.isTrue( null ==sid,"待更新记录不存在!");
        SaleChance temp =selectByPrimaryKey(sid);
        AssertUtil.isTrue( null ==temp,"待更新记录不存在!");
        temp.setDevResult(devResult);
        AssertUtil.isTrue(updateByPrimaryKeySelective(temp)<1,"机会数据更新失败!");
    }
}
