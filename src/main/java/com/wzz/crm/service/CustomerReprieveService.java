package com.wzz.crm.service;

import com.wzz.crm.base.BaseService;
import com.wzz.crm.dao.CustomerLossMapper;
import com.wzz.crm.dao.CustomerReprieveMapper;
import com.wzz.crm.domain.CustomerReprieve;
import com.wzz.crm.utils.AssertUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * 暂缓流失客户服务类
 */
@Service
public class CustomerReprieveService extends BaseService<CustomerReprieve, Integer> {

    @Autowired
    private CustomerReprieveMapper customerReprieveMapper;

    @Autowired
    private CustomerLossMapper customerLossMapper;

    /**
     * 添加暂缓措施
     *  1.数据校验
     *      lossId 非空 存在
     *      暂缓措施内容 非空
     *  2.设置默认值
     *      有效值
     *      创建和更新时间
     *  3.执行添加操作，判断返回值
     * @param customerReprieve  暂缓信息
     */
    @Transactional(propagation = Propagation.REQUIRED)
    public void addCustomerReprieve(CustomerReprieve customerReprieve) {
        //校验lossId
        AssertUtil.isTrue(null == customerReprieve.getLossId()
                || null == customerLossMapper.selectByPrimaryKey(customerReprieve.getLossId()), "流失客户不存在！");
        //校验内容
        AssertUtil.isTrue(StringUtils.isBlank(customerReprieve.getMeasure()), "暂缓措施的内容不能为空！");
        //设置默认值
        customerReprieve.setIsValid(1);
        customerReprieve.setCreateDate(new Date());
        customerReprieve.setUpdateDate(new Date());
        //执行更新操作，判断返回结果
        AssertUtil.isTrue(customerReprieveMapper.insertSelective(customerReprieve) != 1, "暂缓措施添加失败！");
    }
    /**
     * 修改暂缓措施
     *  1.数据校验
     *      id id不能为空 存在
     *      lossId 非空  存在
     *      暂缓措施内容 非空
     *  2.设置默认值
     *      更新时间
     *  3.执行修改操作，判断返回值
     * @param customerReprieve  暂缓信息
     */
    @Transactional(propagation = Propagation.REQUIRED)
    public void updateCustomerReprieve(CustomerReprieve customerReprieve) {
        //校验id
        AssertUtil.isTrue(null == customerReprieve.getId()
        || null ==customerReprieveMapper.selectByPrimaryKey(customerReprieve.getId()),"id不符合！");
        //校验lossId
        AssertUtil.isTrue(null == customerReprieve.getLossId()
                || null == customerLossMapper.selectByPrimaryKey(customerReprieve.getLossId()), "流失客户不存在！");
        //校验内容
        AssertUtil.isTrue(StringUtils.isBlank(customerReprieve.getMeasure()), "暂缓措施的内容不能为空！");
        //设置默认值
        customerReprieve.setUpdateDate(new Date());
        //执行更新操作，判断返回结果
        AssertUtil.isTrue(customerReprieveMapper.updateByPrimaryKeySelective(customerReprieve) != 1, "暂缓措施添加失败！");
    }

    /**
     * 删除暂缓措施（逻辑删除）
     *  修改有效值
     * @param id    待删除的id
     */
    @Transactional(propagation = Propagation.REQUIRED)
    public void deleteById(Integer id) {
        AssertUtil.isTrue(null == id, "待删除记录不存在（id为空）！");
        CustomerReprieve customerReprieve = customerReprieveMapper.selectByPrimaryKey(id);
        AssertUtil.isTrue(null == customerReprieve, "待删除记录不存在！");
        customerReprieve.setIsValid(0);
        customerReprieve.setUpdateDate(new Date());
        AssertUtil.isTrue(customerReprieveMapper.updateByPrimaryKeySelective(customerReprieve) != 1, "删除失败！");
    }
}
