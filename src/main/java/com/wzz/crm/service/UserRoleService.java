package com.wzz.crm.service;

import com.wzz.crm.base.BaseService;
import com.wzz.crm.dao.UserRoleMapper;
import com.wzz.crm.domain.UserRole;
import org.springframework.stereotype.Service;

/**
 * 用户角色关系服务类
 */
@Service
public class UserRoleService extends BaseService<UserRole,Integer> {


}
