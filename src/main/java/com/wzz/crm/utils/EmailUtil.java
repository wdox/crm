package com.wzz.crm.utils;

import org.apache.commons.lang3.StringUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 邮箱校验类
 */
public class EmailUtil {

    /**
     * 邮箱格式校验方法
     * @param email     邮箱
     */
    public static boolean  isEmail(String email) {

        String regex = "^(\\w+([-.][A-Za-z0-9]+)*){3,18}@\\w+([-.][A-Za-z0-9]+)*\\.\\w+([-.][A-Za-z0-9]+)*$";
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(email);
        boolean isMatch = m.matches();
        return isMatch;
    }

    /**
     * 测试
     */
    public static void main(String[] args) {
        System.out.println(isEmail("123@126.com"));

    }
}
