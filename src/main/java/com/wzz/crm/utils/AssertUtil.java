package com.wzz.crm.utils;

import com.wzz.crm.exceptions.ParamsException;

public class AssertUtil {
    public  static void isTrue(Boolean flag,String msg){
        if(flag){
            throw  new ParamsException(msg);
        }
    }
}
