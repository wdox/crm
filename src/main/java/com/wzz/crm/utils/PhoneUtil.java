package com.wzz.crm.utils;

import org.apache.commons.lang3.StringUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 手机号码工具类
 */
public class PhoneUtil {

    /**
     * 判断电手机号码的格式
     * @param phone 手机号码
     * @return
     */
    public static  boolean isMobile(String phone){
        Pattern p = null;
        Matcher m = null;
        boolean b = false;
        // 验证手机号
        //前三位判断，长度判断
        String s2="^[1](([3|5|8][\\d])|([4][4,5,6,7,8,9])|([6][2,5,6,7])|([7][^9])|([9][1,8,9,5]))[\\d]{8}$";
        if(StringUtils.isNotBlank(phone)){
            p = Pattern.compile(s2);
            m = p.matcher(phone);
            b = m.matches();
        }
        return b;
    }

    //测试
    public static void main(String[] args) {
        System.out.println(isMobile("19599999999"));
    }
}
