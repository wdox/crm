package com.wzz.crm.model;

/**
 * 用户返回数据模板
 */
public class UserModel {
   /* Integer id;         //用户数据库主键*/
    //这里因为需要保存Cookie，所以要加密处理
    String userIdStr;
    String userName;    //用户名
    String trueName;    //用户真是姓名

    /*public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }*/

    public String getUserIdStr() {
        return userIdStr;
    }

    public void setUserIdStr(String userIdStr) {
        this.userIdStr = userIdStr;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getTrueName() {
        return trueName;
    }

    public void setTrueName(String trueName) {
        this.trueName = trueName;
    }
}
