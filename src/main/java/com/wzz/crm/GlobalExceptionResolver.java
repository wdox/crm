package com.wzz.crm;

import com.alibaba.fastjson.JSON;
import com.wzz.crm.base.ResultInfo;
import com.wzz.crm.exceptions.AuthException;
import com.wzz.crm.exceptions.NoLoginException;
import com.wzz.crm.exceptions.ParamsException;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.annotation.Annotation;

/**
 * 全局异常统一处理
 * 全局异常处理器
 */
@Component
public class GlobalExceptionResolver implements HandlerExceptionResolver {

    /**
     * 方法返回值类型
     *      1.视图
     *      2.JSON
     * 如何判断方法的返回类型：
     * 如果方法级别配置了 @ResponseBody 注解，表示方法返回的是JSON；
     * 反之，返回的是视图页面
     * @param request       请求对象
     * @param response      响应对象
     * @param handler       方法对象
     * @param ex            异常对象
     * @return
     */

    @Override
    public ModelAndView resolveException(HttpServletRequest request,
                             HttpServletResponse response, Object handler, Exception ex) {
        //在 登录信息检查拦截器 中如果请求未登录会抛出 未登录异常NoLoginException 我们在这里处理以下异常
        if(ex instanceof NoLoginException){
            System.out.println("用户未登录，重定向到登录");
            ModelAndView modelAndView = new ModelAndView("redirect:/index");
            return modelAndView;
        }


        //返回类型为ModelAndView,设置默认异常处理
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("");
        modelAndView.addObject("code",400);
        modelAndView.addObject("msg","系统异常，请重试");

        //判断HandlerMethod
        if(handler instanceof HandlerMethod){
            //类型转换
            HandlerMethod handlerMethod= (HandlerMethod) handler;
            //获取方法上的注解
            /**
             * getMethodAnnotation
             * 获取方法注解
             * Params:
             * annotationType ：the type of annotation to introspect the method for
             * Returns:
             * the annotation, or null if none found
             * 参数：
             * annotationType ：自省方法的注解类型
             * 回报：
             * 注释，如果没有找到，则返回 null
             */
            ResponseBody responseBody =handlerMethod.getMethodAnnotation(ResponseBody.class);
            //判断responseBody注解是否存在（如果不存在则返回视图，如果存在则返回json）
            if(null==responseBody){
                //返回视图
                //判断是否是自定义参数异常
                if (ex instanceof ParamsException){
                    ParamsException paramsException= (ParamsException) ex;
                    modelAndView.addObject("code",paramsException.getCode());
                    modelAndView.addObject("msg",paramsException.getMsg());
                }else if (ex instanceof AuthException){ //权限认证异常
                    AuthException authException= (AuthException) ex;
                    modelAndView.addObject("code",authException.getCode());
                    modelAndView.addObject("msg",authException.getMsg());
                }
                return  modelAndView;
            }else{
                /**
                 * 返回json数据
                 */
                //设置默认异常处理
                ResultInfo resultInfo = new ResultInfo();
                resultInfo.setCode(300);
                resultInfo.setMsg("系统异常，请重试！");
                //判断是否是自定义参数异常，如果是设置异常参数
                if (ex instanceof ParamsException){
                    ParamsException paramsException= (ParamsException) ex;
                    resultInfo.setCode(paramsException.getCode());
                    /*resultInfo.setMsg("全局异常处理器："+paramsException.getMsg());*/
                    resultInfo.setMsg(paramsException.getMsg());
                } else if (ex instanceof AuthException){ //权限认证异常
                    AuthException authException= (AuthException) ex;
                    resultInfo.setCode(authException.getCode());
                    /*resultInfo.setMsg("全局异常处理器："+paramsException.getMsg());*/
                    resultInfo.setMsg(authException.getMsg());
                }

                //使用输出流返回
                //设置响应类型和JSON格式
                response.setContentType("application/json;charset=utf-8");
                //得到输出流
                PrintWriter writer = null;
                try {
                    writer = response.getWriter();
                    writer.write(JSON.toJSONString(resultInfo));
                    //刷新缓冲区
                    writer.flush();
                } catch (IOException e) {
                    e.printStackTrace();
                }finally {
                    if(null != writer){
                        writer.close();
                    }
                }
                return null;
            }

        }
        return modelAndView;
    }
}
