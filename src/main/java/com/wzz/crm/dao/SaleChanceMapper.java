package com.wzz.crm.dao;

import com.wzz.crm.base.BaseMapper;
import com.wzz.crm.domain.SaleChance;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SaleChanceMapper extends BaseMapper<SaleChance,Integer> {
    /**
     * 多条件查询
     * 基类中定义
     * 映射文件中没有自动生成相应的sql，这个需要自己编写
     */

    /**
     * 添加记录
     * 基类定义
     * 相应的sql自动生成
     */


}