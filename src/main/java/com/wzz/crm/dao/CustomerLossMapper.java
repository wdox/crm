package com.wzz.crm.dao;

import com.wzz.crm.base.BaseMapper;
import com.wzz.crm.domain.CustomerLoss;
import org.apache.ibatis.annotations.Mapper;

/**
 *  流失客户的数据访问接口
 */
@Mapper
public interface CustomerLossMapper extends BaseMapper<CustomerLoss,Integer> {

}