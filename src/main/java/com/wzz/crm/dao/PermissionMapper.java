package com.wzz.crm.dao;

import com.wzz.crm.base.BaseMapper;
import com.wzz.crm.domain.Permission;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 权限持久层
 */
@Mapper
public interface PermissionMapper extends BaseMapper<Permission,Integer> {

    /**
     * 查询角色含有的权限个数
     * @param roleId    角色id
     * @return          角色有的权限个数
     */
    int countPermissionByRoleId(Integer roleId);

    /**
     * 根据角色id删除权限
     * @param roleId    角色id
     * @return          受影响的行数
     */
    int deletePermissionByRoleId(Integer roleId);


    /**
     * 查询当前角色的所有权限的id集合
     * @param roleId    角色id
     * @return          权限的id集合
     */
    List<Integer> selectModuleIdsByRoleId(Integer roleId);

    //通过用户id查询当前用的所有权限码
    List<String> queryAllAclValueByUserId(Integer userId);

    //通过模块id查询绑定的权限总数
    Integer countPermissionByModuleId(Integer moduleId);

    //通过moduleId删除权限记录
    Integer deleteByModuleId(Integer moduleId);
}