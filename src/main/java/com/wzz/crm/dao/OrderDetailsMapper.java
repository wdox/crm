package com.wzz.crm.dao;

import com.wzz.crm.base.BaseMapper;
import com.wzz.crm.domain.OrderDetails;
import org.apache.ibatis.annotations.Mapper;

/**
 * 订单详情数据访问接口
 */
@Mapper
public interface OrderDetailsMapper extends BaseMapper<OrderDetails,Integer> {

}