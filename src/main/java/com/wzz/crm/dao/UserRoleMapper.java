package com.wzz.crm.dao;


import com.wzz.crm.base.BaseMapper;
import com.wzz.crm.domain.UserRole;
import org.apache.ibatis.annotations.Mapper;

/**
 * 用户角色关系类
 */
@Mapper
public interface UserRoleMapper extends BaseMapper<UserRole,Integer> {

    /**
     * 查询id用户的角色数量
     * @param userId    用户的id
     * @return          角色数量
     */
    int countUserRoleByUserId(Integer userId);

    /**
     * 删除id用户的所有角色
     * @param userId    待删除的用户的id
     * @return          受影响的记录数
     */
    int deleteUserRoleByUserId(Integer userId);
}