package com.wzz.crm.dao;

import com.wzz.crm.base.BaseMapper;
import com.wzz.crm.domain.CustomerOrder;
import org.apache.ibatis.annotations.Mapper;

import java.util.Map;

/**
 * 客户订单
 */
@Mapper
public interface CustomerOrderMapper extends BaseMapper<CustomerOrder,Integer> {

    //通过id查询订单信息（id,order_no,address,status，total）
    Map<String, Object> queryOrderAndTotalById(Integer orderId);

    //通过客户id查询最后的订单记录
    CustomerOrder queryLastOrderByCustomerId(Integer id);
}