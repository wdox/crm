package com.wzz.crm.dao;

import com.wzz.crm.base.BaseMapper;
import com.wzz.crm.domain.Module;
import com.wzz.crm.model.TreeModel;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 模块持久层
 */
@Mapper
public interface ModuleMapper  extends BaseMapper<Module,Integer> {

    /**
     * 查询所有模块
     * @param roleId    角色id
     * @return          全部的模块集合
     */
    List<TreeModel> queryAllModules(Integer roleId);

    //查询所有的菜单（module）的全部数据
    List<Module> queryModuleList();

    //通过层级和模块名称查询
    Module queryModuleByGradeAndModuleName(Integer grade, String moduleName);

    //通过层级和url查询
    Module queryModuleByGradeAndUrl(Integer grade, String url);

    //通过权限码查询
    Module queryModuleByOptValue(String optValue);

    //通过父菜单id查询
    Module queryModuleByParentId(Integer ParentId);
}