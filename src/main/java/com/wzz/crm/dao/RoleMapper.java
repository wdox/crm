package com.wzz.crm.dao;

import com.wzz.crm.base.BaseMapper;
import com.wzz.crm.domain.Role;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface RoleMapper extends BaseMapper<Role,Integer> {

    /**
     * 查询所有角色
     */
    List<Map<String, Object>> queryAllRoles(Integer userId);

    /**
     * 通过角色名称查询角色
     * @param roleName  角色名称
     * @return          查询到的角色
     */
    Role selectByRoleName(String roleName);

}