package com.wzz.crm.dao;

import com.wzz.crm.base.BaseMapper;
import com.wzz.crm.domain.CusDevPlan;
import org.apache.ibatis.annotations.Mapper;

/**
 * 客户开发计划mapper
 */
@Mapper
public interface CusDevPlanMapper extends BaseMapper<CusDevPlan,Integer> {

}