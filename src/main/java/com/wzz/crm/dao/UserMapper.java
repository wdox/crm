package com.wzz.crm.dao;

import com.wzz.crm.base.BaseMapper;
import com.wzz.crm.domain.User;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;


@Mapper
public interface UserMapper extends BaseMapper<User,Integer> {

    public User queryUserByName(String userName);

    // 查询所有的销售人员
    public List<Map<String, Object>> queryAllSales();

    //查询所有的客户经理
    List<Map<String, Object>> queryAllCustomerManagers();
}