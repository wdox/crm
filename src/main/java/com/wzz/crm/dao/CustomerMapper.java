package com.wzz.crm.dao;

import com.wzz.crm.base.BaseMapper;
import com.wzz.crm.domain.Customer;
import com.wzz.crm.query.CustomerQuery;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 客户持久层
 */
@Mapper
public interface CustomerMapper extends BaseMapper<Customer,Integer> {

    //通过客户名称查询
    Customer queryCustomerByName(String name);

    //查询符合流失的客户
    List<Customer> queryLossCustomers();

    //批量修改流失状态
    int updateStateBatchByIds(List<Integer> ids);

    //多条件分页查询查询贡献值 客户列表
    List<Map<String,Object>> queryCustomerContributionByParams(CustomerQuery customerQuery);

    //查寻客户构成
    List<Map<String, Object>> countCustomerMake();
}