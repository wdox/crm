package com.wzz.crm.dao;

import com.wzz.crm.base.BaseMapper;
import com.wzz.crm.domain.CustomerServe;
import org.apache.ibatis.annotations.Mapper;

/**
 * 客户服务的数据访问接口
 */
@Mapper
public interface CustomerServeMapper extends BaseMapper<CustomerServe,Integer> {

}