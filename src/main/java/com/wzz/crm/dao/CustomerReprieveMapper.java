package com.wzz.crm.dao;

import com.wzz.crm.base.BaseMapper;
import com.wzz.crm.domain.CustomerReprieve;
import org.apache.ibatis.annotations.Mapper;

/**
 * 暂缓流失客户数据访问接口
 */
@Mapper
public interface CustomerReprieveMapper extends BaseMapper<CustomerReprieve,Integer> {

}