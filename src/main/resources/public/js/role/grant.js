$(function () {
    // 加载树形结构
    loadModuleData();
});

// 定义树形结构对象
var zTreeObj;


/**
 * 加载资源树形数据
 */
function loadModuleData() {
    // 配置信息对象  zTree的参数配置
    var setting = {
        // 使用复选框
        check:{
            enable:true
        },
        // 使用简单的JSON数据
        data:{
            simpleData:{
                enable: true
            }
        },
        // 绑定函数
        callback: {
            // onCheck函数：当 checkbox/radio 被选中或取消选中时触发的函数
            onCheck: zTreeOnCheck
        }
    };
    // 数据
    // 通过ajax查询资源列表
    $.ajax({
        type:"get",
        url:ctx + "/module/queryAllModules",
        // 查询所有的资源列表时，传递角色ID，查询当前角色对应的已经授权的资源
        data:{
            roleId:$("[name='roleId']").val()
        },
        dataType:"json",
        success:function (data) {
            // data:查询到的资源列表
            // 加载zTree树插件
            zTreeObj = $.fn.zTree.init($("#test1"), setting, data);
        }
    });
}
var mIds =new Array();
/**
 * 当 checkbox/radio 被选中或取消选中时触发的函数
 * @param event
 * @param treeId
 * @param treeNode
 */
function zTreeOnCheck(event, treeId, treeNode) {
    // alert(treeNode.tId + ", " + treeNode.name + "," + treeNode.checked);

    // getCheckedNodes(checked):获取所有被勾选的节点集合。
    // 如果checked=true，表示获取勾选的节点；如果checked=false，表示获取未勾选的节点。
    var nodes = zTreeObj.getCheckedNodes(true);
     //console.log(nodes);
    // 获取所有的资源的id值  mIds=1&mIds=2&mIds=3
    // 判断并遍历选中的节点集合
    if (nodes.length > 0) {
        // 定义资源ID
        //var mIds = "mIds=";
        // 遍历节点集合，获取资源的ID
        for (var i = 0; i < nodes.length; i++) {
            mIds[i] = nodes[i].id;// 得到复选框的值
        }
       // console.log(mIds);
    }
    // 获取需要授权的角色ID的值（隐藏域）
    //var roleId = $("[name='roleId']").val();

    // 发送ajax请求，执行角色的授权操作
    /*$.ajax({
        type:"post",
        url:ctx + "/role/addGrant",
        data:mIds+"&roleId="+roleId,
        dataType:"json",
        success:function (data) {
            console.log(data);
        }
    });*/
};

layui.use(['form', 'layer', 'formSelects'], function () {
    var form = layui.form,
        layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery;
    /**
     * 表单Submit监听
     */
    form.on('submit(addRoleModule)', function (data) {

        // 提交数据时的加载层 （https://layer.layui.com/）
        var index = top.layer.msg("数据提交中,请稍后...", {
            icon: 16, // 图标
            time: false, // 不关闭
            shade: 0.8 // 设置遮罩的透明度
        });
        console.log(mIds)
        //获取需要授权的角色ID的值（隐藏域）
        var roleId = $("[name='roleId']").val();
        // 请求的地址
        var url = ctx + "/role/addGrant"; // 添加操作

        $.ajax({
                type:"post",
                url:ctx + "/role/addGrant",
                traditional: true,
                data:{
                    mIds:mIds,
                    roleId:roleId
                },
                dataType:"json",
                success (result) {
                    // 判断操作是否执行成功 200=成功
                    if (result.code == 200) {
                        // 成功
                        // 提示成功
                        top.layer.msg(result.msg,{icon:6});
                        // 关闭加载层
                        top.layer.close(index);
                        // 关闭弹出层
                        layer.closeAll("iframe");
                        // 刷新父窗口，重新加载数据
                        parent.location.reload();
                    } else {
                        // 失败
                        layer.msg(result.msg, {icon:5});
                    }
                }
            });

        // 阻止表单提交
        return false;
    });
});