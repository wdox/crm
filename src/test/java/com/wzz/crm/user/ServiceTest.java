package com.wzz.crm.user;

import com.wzz.crm.domain.User;
import com.wzz.crm.service.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class ServiceTest {

    @Autowired
    private UserService userService;

    @Test
    public void selectByPrimaryKeyTest(){
        User user = userService.selectByPrimaryKey(10);
        if (null!=user){
            System.out.println(user.toString());
        }else {
            System.out.println("查询失败！");
        }
    }
}
