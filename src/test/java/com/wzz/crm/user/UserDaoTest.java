package com.wzz.crm.user;

import com.wzz.crm.dao.UserMapper;
import com.wzz.crm.domain.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.Map;

@SpringBootTest
class UserDaoTest {

    @Autowired
    private UserMapper userMapper;

    @Test
    void contextLoads() {
        User user = userMapper.queryUserByName("admin");
        if (null!=user){
            System.out.println(user.toString());
        }else{
            System.out.println("不存在");
        }

    }

    //测试获取说有的销售人员
    @Test
    public void queryAllSalesTest(){
        List<Map<String, Object>> maps = userMapper.queryAllSales();
        System.out.println(maps);
    }

}
