package com.wzz.crm.dao;

import com.wzz.crm.domain.Customer;
import org.junit.jupiter.api.Test;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
class CustomerMapperTest {

    @Autowired
    private CustomerMapper customerMapper;

    @Test
    void queryCustomerByName() {
        Customer customer = customerMapper.queryCustomerByName("北京大牛科技");
        if (null != customer) {
            System.out.println(customer.getId());
        }

    }
}