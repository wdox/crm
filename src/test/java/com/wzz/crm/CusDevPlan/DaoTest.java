package com.wzz.crm.CusDevPlan;

import com.sun.webkit.CursorManager;
import com.wzz.crm.dao.CusDevPlanMapper;
import com.wzz.crm.domain.CusDevPlan;
import com.wzz.crm.query.CusDevPlanQuery;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class DaoTest {

    @Autowired
    private CusDevPlanMapper mapper;

    /**
     * 测试多条件查询
     */
    @Test
    public void selectByParamsTest(){
        CusDevPlanQuery cusDevPlanQuery = new CusDevPlanQuery();
        cusDevPlanQuery.setSid(97);
        List<CusDevPlan> cusDevPlans = mapper.selectByParams(cusDevPlanQuery);
        for (CusDevPlan cusDevPlan : cusDevPlans) {
            System.out.println(cusDevPlan.toString());
        }
    }
}
